
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

$(document).ready(function () {
    $("#btn_get").click(function () {
        var date = $("#date").val();
        console.log(date);
        //var end = $("#endDate").val();
        var formData = new FormData($('#tran_form')[0]);
        $.ajax({
            //url: baseUrl + "/app/console/get_user_data",
            url: baseUrl + "/feeder/public/transactionSummary",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                  $("#result").html("");
                console.log(data);
                if (data.length ==0) {
                    alert("no data");

                } else {

                    var str = "";
                    $.each(data, function (idx, obj) {

                        var username = obj.Username;
                        var Time = obj.Time;
                        var d = obj.Details;
                        var GameCode = obj.GameCode;
                        var Amount = obj.Amount;
                        var Result = obj.Result;
                        //var detail = JSON.parse(obj.Details);

                        str += "Username: " + username + "<br>";
                        str += "GameCode: " + GameCode + "<br>";
                        str += "Time: " + Time + "<br>";
                        str += "Amount: " + Amount + "<br>";
                        str += "result: " + Result + "<br>";
                        str += "detail: <br>";
                        var detail = d.split('"');
                        var i;

                        for (i = 0; i < detail.length; i++) {
                            if ((i % 2) != 0) {
                                str += " : ";
                            }
                            str += detail[i];
                            if ((i % 2) === 0) {
                                str += " <br>"
                            }
                        }


                        str += "<hr>";

                        console.log(username);
//                    console.log(Time);
//                    console.log(detail);
                    });
                    $("#result").css("display", "block");
                    $("#result").html(str);
                }
            }
        });

    });
});