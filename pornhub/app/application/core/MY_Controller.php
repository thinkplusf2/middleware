<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use \Monolog\Formatter\LineFormatter;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class MY_Controller extends CI_Controller {

    protected $data = Array(); //protected variables goes here its declaration
// var $url_joker = 'https://www.superbo68.net';
    var $url_joker = 'http://api.joker688.net:81/';
    var $secret = "qpdcrnoddr7m6";
    var $url_forward = "http://www.joker688.net/";

//var $url_joker = 'http://www.joker688.net';


    function __construct() {

        parent::__construct();
        $this->output->enable_profiler(FALSE); // I keep this here so I dont have to manualy edit each controller to see profiler or not        
//load helpers and everything here like form_helper etc
    }

    public function signature_gen( $array,$passkey) {

        $currency = $array['currency'];
        $token = $array['externalToken'];
        $gameType = $array['gameType'];
        $lang = $array['lang'];
        $nickname = $array['nickname'];
        $playerId = $array['playerID'];
        $siteId = $array['siteID'];
        $skinId = $array['skinID'];
        $passkey = $passkey;

        $cipterText = "${currency}${token}${gameType}${lang}${nickname}${playerId}${siteId}${skinId}${passkey}";
      
        $signature = strtoupper(hash("SHA512", $cipterText));

        return $signature;
    }

    public function gen_uuid() {
        return sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function clean($string) {
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        //return preg_replace('~&#x([0-9A-F]+);~i', '', $string); // Removes special chars.
        return strtoupper($string);
    }

    public function porn_log($funcName, $state, $details, $game) {
        $date = date('d-m-Y');
        $logfile = 'D:/log/pornhub/Log_Pornhub_' . $date . '.log';
        $logger = new Logger('Pornhub_log');
// Now add some handlers
        $logger->pushHandler(new StreamHandler($logfile, Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());
        $msg = "[REFUND] [PORNHUB] Details : {TransactionID: tranID , PlayerID: player, Amount: amount, GameID: gameID, gtype: gtp}";
        $msg = "[WIN] [PORNHUB] Details : { error_type : 401 ACCESS_DENIED, playerID : playerID }";
        $msg = "[BALANCE] [PORNHUB] Details : { error_type : 402 INSUFFICIENT_FUNDS, playerID : playerID }";

        $msg = "[BALANCE] [PORNHUB] Details : { error_type : 403 TOKEN_EXPIRED, playerID : playerID }";

        $msg = "[BALANCE] [PORNHUB] Details : { error_type : 404 TOKEN_NOT_FOUND, playerID : playerID }";
        $msg = "[TIP] [ERROR] [PORNHUB] Details : { error_type : 500 INTERNAL_SERVER_ERROR, playerID : playerID }";

        $msg = "[$funcName] [$game] Details : { $details }";
// You can now use your logger

        switch ($state) {
            case "info":
                $logger->info($msg . "\n");
                break;
            case "debug":
                $logger->debug($msg . "\n");
                break;
            case "alert":
                $logger->alert($msg . "\n");
                break;
            case "critical":
                $logger->critical($msg . "\n");
                break;
            case "emergency":
                $logger->EMERGENCY($msg . "\n");
                break;
            case "error":
                $logger->ERROR($msg . "\n");
                break;
            default:
        }
    }

}
