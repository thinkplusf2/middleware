<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;

//use Wrapper\JokerApiWrapper;
require 'vendor/autoload.php';

class wallet extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('customautoloader');
        $this->load->model('single_wallet_model');
    }

    public function index() {
        $this->load->view('welcome_message');
    }

    public function getBalances() {
        $token = $this->input->post("token");
        echo json_encode(array("success" => true));
    }
    
    public transferin(){
        
    }

    public function deposits() {
        header('Content-Type: application/json');
        $username = $this->input->post("username");
        $amount = $this->input->post("amount");
        $type = $this->input->post("type");

        $array = array(
            "username" => $username,
            "balance" => $amount,
            "type" => $type
        );
        
        $res = $this->single_wallet_model->deposit($array);
//        echo "hey";
        //var_dump($res);
        if ($res == true) {
            echo json_encode(array("success" => true));
        } else {
            echo json_encode(array("success" => false));
        }
    }

//    public function withDraw() {
//        header('Content-Type: application/json');
//        $username = $this->input->post("username");
//        $type = $this->input->post("type");
//
//        $array = array(
//            "username" => $username,
//            "type" => $type
//        );
//
//        $res = $this->single_wallet_model->withdraw($array);
////        echo "hey";
//        //var_dump($res);
//        if ($res == true) {
//            echo json_encode(array("success" => true));
//        } else {
//            echo json_encode(array("success" => false));
//        }
//    }

    public function getBalance() {
        header('Content-Type: application/json');

        $username = $this->input->post("Username");
        $type = "joker";
        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->GetCredit($username);
        echo json_encode($result);
    }

    public function getForWithDraw($username) {

        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->GetCredit($username);
        return $result['Credit'];
    }

    public function deposit() {
        header('Content-Type: application/json');

        $username = $this->input->post("username");
        $amount = $this->input->post("amount");
        $type = $this->input->post("type");

        $array = array(
            "username" => $username,
            "balance" => $amount,
            "type" => $type
        );

        $res = $this->single_wallet_model->deposit($array);

        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->TransferCredit($amount, $RequestID, $username);

        echo json_encode($result);
    }

    public function withdraw() {
        header('Content-Type: application/json');


        $RequestID = $this->input->post("RequestID");
        $username = $this->input->post("username");
        // $type = $this->input->post("type");
        $amount = -($this->getForWithDraw($username));
        $type = $this->input->post("type");

        $array = array(
            "username" => $username,
            "type" => $type
        );

        $res = $this->single_wallet_model->withdraw($array);

        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->TransferCredit($amount, $RequestID, $username);
        echo json_encode($result);
    }

    public function deposit_console() {
        header('Content-Type: application/json');

         //$RequestID = $this->input->post("RequestID");
        $username = $this->input->post("username");
        // $type = $this->input->post("type");
        $amount = $this->input->post("amount");
        $type = $this->input->post("type");

        $array = array(
            "username" => $username,
            "type" => $type,
            "balance" => $amount
        );
         //var_dump($array);
        $res = $this->single_wallet_model->deposit($array);
        
        echo json_encode($res);
    }

    public function withdraw_console() {
        header('Content-Type: application/json');


        //$RequestID = $this->input->post("RequestID");
        $username = $this->input->post("username");
        // $type = $this->input->post("type");
        $amount = $this->input->post("amount");
        $type = $this->input->post("type");

        $array = array(
            "username" => $username,
            "type" => $type,
            "balance" => $amount
        );
        
        //var_dump($array);

        $res = $this->single_wallet_model->withdraw($array);


        echo json_encode($res);
    }

}
