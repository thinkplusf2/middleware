<?php

use GuzzleHttp\Client;

//use Wrapper\JokerApiWrapper;
require 'vendor/autoload.php';

class pornhub_model extends CI_Model {

    public function __construct() {
        parent::__construct();
// $db2 = $this->load->database('middleware_db', TRUE);
    }

    public function bet($array) {
        $db2 = $this->load->database("middleware_db", true);
        $queue = $this->db->select("*")
                        ->from("bet")->where("transactionID", $array["transactionID"])->get();
        $amount = $array['amount'];
        $username = $this->get_username($array['playerID']);
        $arr = array(
            "username" => $username,
            "type" => "pornhub"
        );
        $temp = $this->get_credit($arr);

        $balance = $temp['balance'];

        if ($queue->num_rows() > 0) {
            $res = array(
                "error" => "",
                "balance" => $balance,
                "transactionId" => $array['transactionID']
            );
            return $res;
        } else {
            $this->db->insert("bet", $array);

            $balance = $balance - $amount;

            if ($balance <= 0) {
                $res = array(
                    "error" => "402",
                    "code" => "INSUFFICIENT_FUNDS",
                    "message" => "Player has insufficient funds"
                );
                return $res;
            }

            $db2->set(array("balance" => $balance));
            $db2->where(array("username" => $username, "type" => "pornhub"));
            $db2->update('wallet');

            $res = array(
                "error" => "",
                "balance" => $balance,
                "transactionId" => $array['transactionID']
            );
            return $res;
        }
    }

    public function win($array) {
        $db2 = $this->load->database("middleware_db", true);
        $queue = $this->db->select("*")
                        ->from("win")->where("transactionID", $array["transactionID"])->get();
        $amount = $array['amount'];
        $username = $this->get_username($array['playerID']);
        $arr = array(
            "username" => $username,
            "type" => "pornhub"
        );
        $temp = $this->get_credit($arr);
        $balance = $temp['balance'];

        if ($queue->num_rows() > 0) {
            $res = array(
                "error" => "",
                "balance" => $balance,
                "transactionId" => $array['transactionID']
            );
            return $res;
        } else {
            $this->db->insert("win", $array);

            $balance = $balance + $amount;

            $db2->set(array("balance" => $balance));
            $db2->where(array("username" => $username, "type" => "pornhub"));
            $db2->update('wallet');

            $res = array(
                "error" => "",
                "balance" => $balance,
                "transactionId" => $array['transactionID']
            );
            return $res;
        }
    }

    public function refund($array) {
        $db2 = $this->load->database("middleware_db", true);
        $queue = $this->db->select("*")
                        ->from("refund")->where("transactionID", $array["transactionID"])->get();
        $amount = $array['amount'];
        $username = $this->get_username($array['playerID']);
        $arr = array(
            "username" => $username,
            "type" => "pornhub"
        );
        $temp = $this->get_credit($arr);
        $balance = $temp['balance'];

        if ($queue->num_rows() > 0) {
            $res = array(
                "error" => "",
                "balance" => $balance,
                "transactionId" => $array['transactionID']
            );
            return $res;
        } else {
            $this->db->insert("refund", $array);
            $balance = $balance + $amount;

            $db2->set(array("balance" => $balance));
            $db2->where(array("username" => $username, "type" => "pornhub"));
            $db2->update("wallet");

            $res = array(
                "error" => "",
                "balance" => $balance,
                "transactionId" => $array['transactionID']
            );
            return $res;
        }
    }

    public function get_nickname($playerID) {
        $nickname = "";
        $qu1 = $this->db->select("*")->from("pornhub_user_info")->where(array("playerID" => $playerID))->get();
        if ($qu1->num_rows() > 0) {
            foreach ($qu1->result() as $row) {
                $nickname = $row->nickname;
            }
            $res = array(
                "status" => true,
                "nickname" => $nickname
            );
            return $res;
        } else {
            $res = array(
                "status" => false
            );
            return $res;
        }
    }

    public function tip($array) {
        $db2 = $this->load->database("middleware_db", true);
        $queue = $this->db->select("*")
                        ->from("tip")->where("transactionID", $array["transactionID"])->get();
        $amount = $array['amount'];

        $username = $this->get_username($array['playerID']);
        $arr = array(
            "username" => $username,
            "type" => "pornhub"
        );
        $temp = $this->get_credit($arr);
        $balance = $temp['balance'];
        if ($queue->num_rows() > 0) {
            $res = array(
                "error" => "",
                "balance" => $balance,
                "transactionId" => $array['transactionID']
            );
            return $res;
        } else {
            $this->db->insert("tip", $array);
            $balance = $balance - $amount;

            $db2->set(array("balance" => $balance));
            $db2->where(array("username" => $username, "type" => "pornhub"));
            $db2->update("wallet");

            $res = array(
                "error" => "",
                "balance" => $balance,
                "transactionId" => $array['transactionID']
            );
            return $res;
        }
    }

    public function create_user($array) {
        $check = "0";
        $db2 = $this->load->database('middleware_db', TRUE);
        $data = array(
            "username" => $array['username'],
            "type" => $array['type'],
            "balance" => "0",
            "currenyCode" => "THB"
        );

        $re = $db2->select("*")->from("users")->where("username", $array['username'])->get();

        if ($re->num_rows() == 0) {
            $userData = array(
                "username" => $array['username']
            );
            $qu = $db2->insert("users", $userData);
            $check = "1";
        }

        $where = array(
            "username" => $array['username'],
            "type" => "pornhub"
        );

        $res = $db2->select("*")->from("wallet")->where($where)->get();
        if ($res->num_rows() == 0) {
            $qu1 = $db2->insert("wallet", $data);
            $check = "1";
        }


        $data2 = array(
            "username" => $array['username'],
            "nickname" => $array['nickname']
        );

        $res2 = $db2->select("*")->from("pornhub_user_info")->where("username", $array['username'])->get();
        if ($res2->num_rows() == 0) {
            $qu2 = $db2->insert("pornhub_user_info", $data2);
            $check = "1";
        } else {
            $db2->set($data2);
            $db2->where("username", $array['username']);
            $db2->update("pornhub_user_info");
        }


        if ($check == "1") {
            $res = array(
                "status" => "created"
            );
        } else {
            $res = array(
                "status" => "already have the account"
            );
        }



        return $res;
    }

    public function deposit($array) {
        //var_dump($array);
        $db2 = $this->load->database('middleware_db', TRUE);

        $where = array(
            "username" => $array['username'],
            "type" => $array['type']
        );

        $data = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();
        if ($data->num_rows() == 0) {
            $insert_info = array(
                "username" => $array['username'],
                "type" => $array['type'],
                "balance" => "0",
                "currenyCode" => "THB"
            );

            $db2->insert("wallet", $insert_info);
        }
        foreach ($data->result() as $row) {
            $balance = $row->balance;
        }

        $balance = $balance + $array["balance"];
        //var_dump($array["balance"]);

        $db2->set(array("balance" => $balance));
        $db2->where($where);
        $res = $db2->update('wallet');
//        $data2 = $db2->select('*')->from('wallet')
//                ->where($where)
//                ->get();
        // var_dump($where);
        // var_dump($data2->result());
        if ($res) {
            $data3 = $db2->select('*')->from('wallet')
                    ->where($where)
                    ->get();
            foreach ($data3->result() as $row) {
                $user = $row->username;
                $bal = $row->balance;
                $return = array(
                    "username" => $user,
                    "balance" => $bal
                );
            }
            return $return;
        } else {
            return false;
        }
    }

    public function withdraw($array) {


        $db2 = $this->load->database('middleware_db', TRUE);

        $where = array(
            "username" => $array['username'],
            "type" => $array['type']
        );

        $data = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        if ($data->num_rows() == 0) {
            $insert_info = array(
                "username" => $array['username'],
                "type" => $array['type'],
                "balance" => "0",
                "currenyCode" => "THB"
            );

            $db2->insert("wallet", $insert_info);
        }
        foreach ($data->result() as $row) {
            $balance = $row->balance;
        }
        //echo $balance."<br>";
        //echo $array["balance"];
        $balance = $balance + $array["balance"];
        // echo $balance."<br>";
        if($balance<0){
            $arr = array(
                "error" => "not enough credit"
            );
            return $arr;
        }

        $db2->set(array("balance" => $balance));
        $db2->where($where);
        $res = $db2->update('wallet');
        //var_dump($where);
        $data2 = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        //var_dump($data2->result());
        if ($res) {

            $data3 = $db2->select('*')->from('wallet')
                    ->where($where)
                    ->get();
            foreach ($data3->result() as $row) {
                $user = $row->username;
                $bal = $row->balance;
                $return = array(
                    "username" => $user,
                    "balance" => $bal
                );
            }
            return $return;
        } else {
            $arr = array(
                "error" => "no user or no credits"
            );
            return $arr;
        }
    }

    public function get_credit($array) {
        $db2 = $this->load->database('middleware_db', TRUE);

        $where = array(
            "username" => $array['username'],
            "type" => $array['type']
        );

        $data2 = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        foreach ($data2->result() as $row) {
            $credit = $row->balance;
        }

        if ($credit != null) {
            $arr = array(
                "status" => "200",
                "username" => $array['username'],
                "balance" => $credit
            );
        } else {
            $arr = array(
                "status" => "500",
                "username" => $array['username'],
                "balance" => $credit
            );
        }

        return $arr;
    }

    public function balance($array) {
        $db2 = $this->load->database('middleware_db', TRUE);

        $res = $db2->select("*")->from("pornhub_user_info")->where("playerID", $array['playerID'])->get();
        if ($res->num_rows() > 0) {
            foreach ($res->result() as $row) {
                $where = array(
                    "username" => $row->username,
                    "type" => "pornhub"
                );

                $res2 = $db2->select("*")->from("wallet")->where($where)->get();

                if ($res2->num_rows() > 0) {
                    foreach ($res2->result() as $row2) {
                        return array("balance" => $row2->balance);
                    }
                } else {
                    $data = array(
                        "username" => $row->username,
                        "type" => "pornhub",
                        "balance" => "0",
                        "currenyCode" => "THB"
                    );

                    $db2->insert("wallet", $data);
                    return array("balance" => "0");
                }
            }
        } else {
            return array("status" => "user's not found");
        }
    }

    public function get_username($playerID) {
        $db2 = $this->load->database("middleware_db", true);

        $where2 = array(
            "playerID" => $playerID,
        );
        $queue2 = $db2->select("username")->from("pornhub_user_info")->where($where2)->get();
        $username = "";
        foreach ($queue2->result() as $row) {
            $username = $row->username;
        }

        return $username;
    }

    public function get_pornhub_user($username) {
        $db2 = $this->load->database("middleware_db", true);

        $where = array(
            "username" => $username
        );
        $array = array();
        $playerID = "";
        $nickname = "";
        $qu1 = $db2->select("*")->from("pornhub_user_info")->where($where)->get();
        if ($qu1->num_rows() > 0) {
            foreach ($qu1->result() as $row) {
                $playerID = $row->playerID;
                $nickname = $row->nickname;
            }
            $array = array(
                "status" => "ok",
                "playerID" => $playerID,
                "nickname" => $nickname
            );
        } else {
            $array = array(
                "status" => "error",
                "message" => "no data of pornhub casino of this user"
            );
        }

        return $array;
    }

    public function SaveTransaction($array, $type) {
        $db2 = $this->load->database("middleware_db", true);
        $where_user = array(
            "playerID" => $array['playerID']
        );

        $qu1 = $db2->select("username")->from("pornhub_user_info")->where($where_user)->get();

        if ($qu1->num_rows() > 0) {
            $username = "";
            foreach ($qu1->result() as $row) {
                $username = $row->username;
            }

            $data = array(
                "username" => $username,
                "gameID" => $array['gameID'],
                "transactionType" => $type,
                "amount" => $array['amount'],
                "gameType" => $array['gtype']
            );

            $res = $this->db->insert("transaction", $data);
            
            return 1;
        } else {
            return 0;
        }
    }
 
}
