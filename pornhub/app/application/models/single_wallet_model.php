<?php

use GuzzleHttp\Client;

//use Wrapper\JokerApiWrapper;
require 'vendor/autoload.php';

class single_wallet_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        require 'vendor/autoload.php';
        $this->load->model("single_wallet_model");
// $db2 = $this->load->database('middleware_db', TRUE);
    }

    public function placeBet($array) {
        $db2 = $this->load->database('middleware_db', TRUE);
        $res = $this->db->insert('place_bet', $array);
        $type = "cockfight";



        $where = array(
            "username" => $array['userId'],
            "type" => $type
        );

//var_dump($where);
        $data2 = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        if ($data2->num_rows() > 0) {
            foreach ($data2->result() as $r2) {
                $balance = $r2->balance;
            }
            $balance_cal = $balance - $array['betAmount'];
            if ($balance_cal < 0) {
                $result = array(
                    "status" => "500",
                    "desc" => "balance is not enough"
                );
                return $result;
            } else {

                $db2->set(array("balance" => $balance_cal));


                $db2->where($where);
                $res = $db2->update('wallet');

                $stat_data = array(
                    "old_balance" => $balance,
                    "change_balance" => -$array['betAmount'],
                    "new_balance" => $balance_cal,
                    "game" => "cockfight",
                    "txId" => $array['txId']
                );

                $in_res = $this->db->insert("statement", $stat_data);



                $result = array(
                    "status" => "200",
                    "balance" => $balance_cal
                );
                return $result;
            }
        }
    }

    public function cancelBet($array) {

        $res = $this->db->insert('cancel_bet', $array);

        $db2 = $this->load->database('middleware_db', TRUE);

        $type = "cockfight";


        $where = array(
            "username" => $array['userId'],
            "type" => $type
        );

//var_dump($where);
        $data2 = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        if ($data2->num_rows() > 0) {
            foreach ($data2->result() as $r2) {
                $balance = $r2->balance;
            }
            $balance_cal = $balance + $array['betAmount'];
            if ($balance_cal < 0) {
                $result = array(
                    "status" => "500",
                    "desc" => "balance is not enough"
                );
                return $result;
            } else {

                $db2->set(array("balance" => $balance_cal));


                $db2->where($where);
                $res = $db2->update('wallet');



                $result = array(
                    "status" => "200",
                    "balance" => $balance_cal
                );
                return $result;
            }
        }
    }

    public function updateBet($array) {
        $db2 = $this->load->database('middleware_db', TRUE);
        $type = "cockfight";
        $where = array(
            "username" => $array['userId'],
            "type" => $type
        );
        //var_dump($where);
        $res = $this->db->insert('update_bet', $array);
        $data2 = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        if ($data2->num_rows() > 0) {
            foreach ($data2->result() as $r2) {
                $balance = $r2->balance;
            }
        }
        if ($res) {
            $result = array(
                "status" => "200",
                "balance" => $balance
            );

            return $result;
        } else {
            $result = array(
                "status" => "500",
                "desc" => "system busy"
            );
            return $result;
        }
    }

    public function settleMatch($array) {

        $res = $this->db->insert('settle_match', $array);

        if ($res) {
            $result = array(
                "status" => "200"
            );
            return $result;
        } else {

            $result = array(
                "status" => "500",
                "desc" => "system busy"
            );
            return $result;
        }
    }

    public function Calculate_winloss($array) {
        $db2 = $this->load->database('middleware_db', TRUE);
        $where = array(
            "username" => $array['userId'],
            "type" => "cockfight"
        );
        $res = $db2->select("*")
                        ->from("wallet")
                        ->where($where)->get();
        $newbalance = 0;

        foreach ($res->result() as $row) {


            switch ($array['status']) {
                case "WIN" :
                    $where2 = array(
                        "txId" => $array['txId']
                    );
                    $res2 = $this->db->select("*")
                                    ->from("settle_match")
                                    ->where($where2)->get();

                    if ($res2->num_rows() > 0 && $res2->num_rows() == 1 && $array['winLoss'] == 0) {

                        foreach ($res2->result() as $row2) {

                            if ($row2->status == "LOSE") {
                                $newbalance = $row->balance + ($array['betAmount'] * $array['result'] * $array['odds']);

                                $balance = $row->balance;
                                $change_balance = ($array['betAmount'] * $array['result'] * $array['odds']);
                                //echo $newbalance."W1<br>";
                            }
                        }
                    } else {
                        $newbalance = $row->balance + $array['winLoss'] + $array['betAmount'];
                        $balance = $row->balance;
                        $change_balance = $array['winLoss'] + $array['betAmount'];
                        // echo $newbalance."W2<br>";
                    }

                    break;
                case "VOID" :

                    $newbalance = ($row->balance + ( $array['result'] * ($array['betAmount'] * $array['odds'])) + $array['betAmount']);
                    $balance = $row->balance;
                    $change_balance = ( $array['result'] * ($array['betAmount'] * $array['odds'])) + $array['betAmount'];
                    break;
                case "DRAW" :

                    $where2 = array(
                        "txId" => $array['txId']
                    );
                    $res2 = $this->db->select("*")
                                    ->from("settle_match")
                                    ->where($where2)->get();

                    if ($res2->num_rows() > 0) {
                        $newbalance = $row->balance;
                        $balance = $row->balance;
                        $change_balance = 0;
                    } else {
                        $newbalance = ($row->balance + $array['betAmount']);
                        $balance = $row->balance;
                        $change_balance = $array['betAmount'];
                    }


                    break;
                case "LOSE" :
                    $where2 = array(
                        "txId" => $array['txId']
                    );
                    $res2 = $this->db->select("*")
                                    ->from("settle_match")
                                    ->where($where2)->get();

                    if ($res2->num_rows() > 0 && $res2->num_rows() == 1 && $array['winLoss'] == 0) {

                        foreach ($res2->result() as $row2) {

                            if ($array['winLoss'] == 0) {
                                if ($row2->status == "WIN") {
                                    $newbalance = $row->balance + (-($row2->winLoss));
                                    $balance = $row->balance;
                                    $change_balance = (-($row2->winLoss));
                                    //echo $newbalance."L1<br>";
                                } else if ($row2->status == "LOSE") {

                                    $newbalance = $row->balance + (-($row2->winLoss));
                                    $balance = $row->balance;
                                    $change_balance = (-($row2->winLoss));
                                }
                            }
                        }
                    } else if ($res2->num_rows() == 0) {
                        if ($array['odds'] < 0) {
                            $newbalance = $row->balance + ($array['odds'] * $array['betAmount'] + $array['betAmount']);
                            $balance = $row->balance;
                            $change_balance = ($array['odds'] * $array['betAmount'] + $array['betAmount']);
                        } else {
                            $newbalance = $row->balance;
                            $balance = $row->balance;
                            $change_balance = 0;
                        }

                        //echo $newbalance."L2<br>";
                    } else if ($res2->num_rows() > 1) {
                        $newbalance = $row->balance + $array['winLoss'];
                        $balance = $row->balance;
                        $change_balance = $array['winLoss'];
                    }


                    break;
            }


//                switch ($array['status']) {
//                    case "WIN" :
//                        $where2 = array(
//                            "txId" => $array['txId']
//                        );
//                        
//
//                        
//                            if ($array['odds'] < 0) {
//                                $newbalance = ($row->balance + (2 * $array['betAmount']));
//                            } else {
//                               // echo "WIN2<br>";
//                                $newbalance = ($row->balance + ( $array['result'] * ($array['betAmount'] * $array['odds'])) + $array['betAmount']);
//                                //echo $newbalance."<br>";
//                            }
//                        
//
//
//
//                        break;
//                    case "VOID" :
//
//                        $newbalance = ($row->balance + ( $array['result'] * ($array['betAmount'] * $array['odds'])) + $array['betAmount']);
//                        break;
//                    case "DRAW" :
//                        $newbalance = ($row->balance + $array['betAmount']);
//                        break;
//                    case "LOSE" :
//                        $where2 = array(
//                            "txId" => $array['txId']
//                        );
//                       
//
//                        
//                            if ($array['odds'] < 0) {
//                                // odds - �?ื�?ตั�? %
//                                $newbalance = ($row->balance + ($array['odds'] * $array['betAmount']) + $array['betAmount']);
//                            } else {
//                               // echo "LOSE2";
//                                 //echo $newbalance."<br>";
//                                // odds + �?ื�? %
//                                //$newbalance = ($row->balance + ( $array['result'] * ($array['betAmount'] * $array['odds'])) + $array['betAmount']);
//                            }
//                        
//
//
//                        break;
//                }
            //$newbalance = ($row->balance + ( $array['result'] * ($array['betAmount'] * $array['odds'])));


            if ($newbalance < 0) {
                $result = array(
                    "status" => "500",
                    "desc" => "system busy"
                );
                return $result;
            } else {

                if ($newbalance) {
                    $db2->set(array("balance" => $newbalance));


                    $db2->where($where);
                }

//                var_dump($row->balance);
//                var_dump($newbalance);
                $res = $db2->update('wallet');

                $stat_data = array(
                    "old_balance" => $balance,
                    "change_balance" => $change_balance,
                    "new_balance" => $newbalance,
                    "game" => "cockfight",
                    "txId" => $array['txId']
                );

                $in_res = $this->db->insert("statement", $stat_data);

                $result = array(
                    //"new" => $newbalance,

                    "status" => "200"
                );
                return $result;
            }
        }
    }

    public function voidLateClose($array) {

        $res = $this->db->insert('void_late_close', $array);

        $db2 = $this->load->database('middleware_db', TRUE);
        $where = array(
            "txId" => $array['txId']
        );

        $res2 = $this->db->select("*")
                        ->from("place_bet")
                        ->where($where)->get();
        $refund = "0";
        if ($res2->num_rows() > 0) {
            foreach ($res2->result()as $row) {
                $refund = $row->betAmount;

                $userId = $row->userId;
            }
        } else {
            $result = array(
                "status" => "500",
                "desc" => "system busy"
            );
        }

        $newbalance = "0";
        // echo $userId;
        //echo $balance;
        $where2 = array(
            "username" => $userId,
            "type" => "cockfight"
        );

        $res3 = $db2->select("*")
                        ->from("wallet")
                        ->where($where2)->get();
        //var_dump($res3);

        if ($res3->num_rows() > 0) {
            foreach ($res3->result()as $row2) {
                $newbalance = $row2->balance + $refund;
                $balance = $row2->balance;
                $change_balance = $refund;
            }
        }

        $db2->set(array("balance" => $newbalance));
        $db2->where($where2);
        $res4 = $db2->update('wallet');

        $stat_data = array(
            "old_balance" => $balance,
            "change_balance" => $change_balance,
            "new_balance" => $newbalance,
            "game" => "cockfight",
            "txId" => $array['txId']
        );

        $in_res = $this->db->insert("statement", $stat_data);
        if ($res4) {
            $result = array(
                "status" => "200"
            );
            return $result;
        } else {

            $result = array(
                "status" => "500",
                "desc" => "system busy"
            );
            return $result;
        }
    }

    public function refund($array) {

        $res = $this->db->insert('refund', $array);


        if ($res) {
            $result = array(
                "status" => "200"
            );
            return $result;
        } else {

            $result = array(
                "status" => "500",
                "desc" => "system busy"
            );
            return $result;
        }
    }

    public function test() {
        $db2 = $this->load->database('middleware_db', TRUE);
        $res = $db2->select('*')->from('users')->get();

        foreach ($res->result() as $row) {
            echo $row->username;
            echo $row->password;
        }

        var_dump($res->result());
    }

    public function deposit($array) {
        //var_dump($array);
        $db2 = $this->load->database('middleware_db', TRUE);

        $where = array(
            "username" => $array['username'],
            "type" => $array['type']
        );

        $data = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();
        foreach ($data->result() as $row) {
            $balance = $row->balance;
        }

        $balance = $balance + $array["balance"];
        //var_dump($array["balance"]);

        $db2->set(array("balance" => $balance));
        $db2->where($where);
        $res = $db2->update('wallet');
//        $data2 = $db2->select('*')->from('wallet')
//                ->where($where)
//                ->get();
        // var_dump($where);
        // var_dump($data2->result());
        if ($res) {
            $data3 = $db2->select('*')->from('wallet')
                    ->where($where)
                    ->get();
            foreach ($data3->result() as $row) {
                $user = $row->username;
                $bal = $row->balance;
                $return = array(
                    "username" => $user,
                    "balance" => $bal
                );
            }
            return $return;
        } else {
            return false;
        }
    }

    public function withdraw($array) {


        $db2 = $this->load->database('middleware_db', TRUE);

        $where = array(
            "username" => $array['username'],
            "type" => $array['type']
        );

        $data = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();
        foreach ($data->result() as $row) {
            $balance = $row->balance;
        }
        //echo $balance."<br>";
        //echo $array["balance"];
        $balance = $balance + $array["balance"];
        // echo $balance."<br>";


        $db2->set(array("balance" => $balance));
        $db2->where($where);
        $res = $db2->update('wallet');
        //var_dump($where);
        $data2 = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        //var_dump($data2->result());
        if ($res) {

            $data3 = $db2->select('*')->from('wallet')
                    ->where($where)
                    ->get();
            foreach ($data3->result() as $row) {
                $user = $row->username;
                $bal = $row->balance;
                $return = array(
                    "username" => $user,
                    "balance" => $bal
                );
            }
            return $return;
        } else {
            $arr = array(
                "error" => "no user or no credits"
            );
            return $arr;
        }
    }

    public function get_credit($array) {
        $db2 = $this->load->database('middleware_db', TRUE);

        $where = array(
            "username" => $array['username'],
            "type" => $array['type']
        );



        $data2 = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        foreach ($data2->result() as $row) {
            $credit = $row->balance;
        }

        if ($credit != null) {
            $arr = array(
                "status" => "200",
                "username" => $array['username'],
                "balance" => $credit
            );
        } else {
            $arr = array(
                "status" => "500",
                "username" => $array['username'],
                "balance" => $credit
            );
        }

        return $arr;
    }

    public function create_user($array) {
        $db2 = $this->load->database('middleware_db', TRUE);

        $data = array(
            "username" => $array['user']
        );

        $res = $db2->insert('users', $data);

        //echo $res;
        if ($res) {
            $data2 = array(
                "currenyCode" => "THB",
                "balance" => "0",
                "type" => "joker",
                "username" => $array['user']
            );
            $db2->insert('wallet', $data2);
            $data3 = array(
                "currenyCode" => "THB",
                "balance" => "0",
                "type" => "cockfight",
                "username" => $array['user']
            );
            $db2->insert('wallet', $data3);
            return "Created";
        } else {
            return "Duplicated user's name";
        }
    }

    public function delete_user($username) {
        $db2 = $this->load->database("middleware_db", TRUE);
        $db2->where('username', $username);
        $db2->delete('users');
        $res = array('status' => 'deleted');
        return $res;
    }

    public function update_balance($array) {

        $where = array(
            "username" => $array['Username'],
            "type" => "joker"
        );
        $db2 = $this->load->database('middleware_db', TRUE);

        $db2->set(array("balance" => $array['Credit']));


        $db2->where($where);
        $db2->update('wallet');

        return 1;
    }

    public function update_all_balance($result) {
        $api = new Wrapper\JokerApiWrapper();

        $where = array(
            "type" => "joker"
        );
        $db2 = $this->load->database('middleware_db', TRUE);

        $data = $db2->select('*')->from('wallet')
                ->where($where)
                ->get();

        if ($data->num_rows() > 0) {
            foreach ($data->result() as $row) {

                $result = $api->GetCredit($row->username);
                $where2 = array(
                    "username" => $result['Username'],
                    "type" => "joker"
                );
                $db2->set(array("balance" => $result['Credit']));


                $db2->where($where2);
                $db2->update('wallet');
            }
        }

        return 1;
    }

}
