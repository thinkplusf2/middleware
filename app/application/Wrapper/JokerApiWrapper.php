<?php

namespace Wrapper;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use DateTime;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use \Monolog\Formatter\LineFormatter;

require 'vendor/autoload.php';

class JokerApiWrapper {

    protected $data = Array(); //protected variables goes here its declaration
    // var $url_joker = 'https://www.superbo68.net';
    var $url_joker = 'http://api.joker688.net:81/';
    var $secret = "qpdcrnoddr7m6";
    var $url_forward = "http://www.joker688.net/";

    //var $url_joker = 'http://www.joker688.net';


    function __construct() {
        
    }

    public function signature_gen($secret, $key) {
        //$secret = "qpdcrnoddr7m6"
        // $key must be array ;
        $text = "";
        ksort($key);
        while ($key_name = current($key)) {

            $text .= key($key) . '=' . $key_name . '&';

            next($key);
        }
        // again 

        $text = substr($text, 0, -1); // อั�?�?ี�?�?ือะ�?ร ตัดตัว & สุดท�?ายออ�?�?รั�? ok
        // var_dump($text);
        // echo "<br>";
        // var_dump($secret);
        // echo "<br>";
        $signature = base64_encode(hash_hmac("SHA1", $text, $secret, true));
        // echo "<br>";
        // var_dump($signature);
        // echo "<br>";
        //$signature = base64_encode(hash_hmac("SHA1",$key, $secret));
        //return substr( $signature, 0, -2);
        return $signature;
    }

    public function EnsureUserAccount($username) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        // $api = new JokerApiWrapper();
        // $result = $api->ensureUser(asdf, asdf); 
        //$data = "Method=CU&Timestamp=".$timestamp."&Username=".$username;

        $dataArray = array(
            'Method' => "CU",
            'Timestamp' => "$timestamp",
            'Username' => "$username"
        );

        $signature = $this->signature_gen($this->secret, $dataArray);

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
            'form_params' => array(
                //   'AppID' => "TF39",
                //     "Signature" => "qpdcrnoddr7m6",
                'Method' => "CU",
                'Timestamp' => "$timestamp",
                'Username' => "$username"
            )
        );

        $res = $client->post(
                $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );

//        var_dump($this->url_joker);
//        var_dump($post_data);

        $status_code = $res->getStatusCode();
        

        //echo $res->getBody();
        // var_dump($res);

        if ($status_code == "200") {
            $data = json_decode($res->getBody(), true);
            $status_res = $data['Status'];
            $result = array(
                "Status" => $data['Status']
            );
            

            return $result;
        } else if ($status_code == "201") {
            $result = array(
                "Status" => "Created"
            );
            return $result;
        } else {
            $result = array(
                "error" => "Error"
            );
            return $result;
        }
    }

    public function GetCredit($username) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $dataArray = array(
            'Method' => 'GC',
            'Timestamp' => $timestamp,
            'Username' => $username
        );

        $signature = $this->signature_gen($this->secret, $dataArray);


        $post_data = array(
            'form_params' => array(
                'Method' => 'GC',
                'Timestamp' => $timestamp,
                'Username' => $username
        ));

        $res = $client->post(
                $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(), true);
        // echo $res->getStatusCode();
        // //echo $res->getBody();
        // var_dump($res);
        // if user exists 
        //var_dump($res);
        if ($status_code == "200") {
            $data = json_decode($res->getBody(), true);
            $username_res = $data['Username'];
            $credit_res = $data['Credit'];

            $result = array(
                "Username" => $username_res,
                "Credit" => $credit_res
            );

            return $result;
        } else if ($status_code == "404") {
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        } else {
            return $this->GetCredit($username);
        }
    }

    public function TransferCredit($Amount, $RequestID, $username) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $dataArray = array(
            'Amount' => "$Amount",
            'Method' => 'TC',
            'Timestamp' => $timestamp,
            'Username' => $username
        );
        $dataReq = array();
        if (null != $RequestID) {
            $dataReq = array(
                'RequestID' => $RequestID
            );
        }

        $dataArray = array_merge($dataArray, $dataReq);
        //var_dump($dataArray);
        $signature = $this->signature_gen($this->secret, $dataArray);


        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
            'form_params' => array(
                'Amount' => $Amount,
                'Method' => 'TC',
                'RequestID' => $RequestID,
                'Timestamp' => $timestamp,
                'Username' => $username
        ));

        $res = $client->post(
                $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );


        $status_code = $res->getStatusCode();


        // if user exists 
        if ($status_code == "200") {
            $data = json_decode($res->getBody(), true);
            $result = array(
                "Username" => $data['Username'],
                "Credit" => $data['Credit'],
                "RequestID" => $data['RequestID'],
                "Time" => $data['Time'],
                "BeforeCredit" => $data['BeforeCredit']
            );
            
            if($Amount>=0){
                 $this->joker_log("DEPOSIT return", "info", json_encode($result), "Joker");
                 
            }else{
                 $this->joker_log("WITHDRAW return", "info", json_encode($result), "Joker");
            }

            return $result;
        } else if ($status_code == "404") {
            $result = array(
                "error" => "User does not exist"
            );
             $this->joker_log("Deposit/Withdraw", "error", json_encode($result), "Joker");
            return $result;
        } else if ($status_code == "400"){
            //return array("status" => $res->getStatusCode(),"body" => $res->getBody());
             return $this->TransferCredit($Amount, $RequestID, $username);
        }else{
             return $this->TransferCredit($Amount, $RequestID, $username);
        }
    }

    public function GetCreditTransfer($RequestID) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $dataArray = array(
            'Method' => 'TCH',
            'RequestID' => $RequestID,
            'Timestamp' => $timestamp
        );

        $signature = $this->signature_gen($this->secret, $dataArray);


        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
            'form_params' => array(
                //   'AppID' => "TF39",
                //     "Signature" => "qpdcrnoddr7m6",
                'Method' => 'TCH',
                'RequestID' => $RequestID,
                'Timestamp' => $timestamp
        ));

        $res = $client->post(
                $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );

        $status_code = $res->getStatusCode();



        // if user exists 
        if ($status_code == "200") {
            $data = json_decode($res->getBody(), true);

            $username_res = $data['Username'];
            $requestID_res = $data['RequestID'];
            $time_res = $data['Time'];
            $amount_res = $data['Amount'];


            $result = array(
                "Username" => $data['Username'],
                "Amount" => $data['Amount'],
                "RequestID" => $data['RequestID'],
                "Time" => $data['Time']
            );
            return $result;
        } else if ($status_code == "404") {
            $result = array(
                "error" => "RequestID does not exist"
            );
            return $result;
        } else {
            $result = array(
                "error" => "Error"
            );
            return $result;
        }
    }

    public function RequestUserToken($username) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $dataArray = array(
            'Method' => "RT",
            'Timestamp' => "$timestamp",
            'Username' => "$username"
        );

        $signature = $this->signature_gen($this->secret, $dataArray);


        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
            'form_params' => array(
                'Method' => 'RT',
                'Timestamp' => $timestamp,
                'Username' => $username
        ));

        $res = $client->post(
                $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );

        //echo $res->getBody();

        $status_code = $res->getStatusCode();


        // if user exists 
        if ($status_code == "200") {
            $data = json_decode($res->getBody(), true);

            $username_res = $data['Username'];
            $token_res = $data['Token'];
            $result = array(
                "Username" => $data['Username'],
                "Token" => $data['Token'],
                "status" => "200"
            );
            return $result;
        } else if ($status_code == "404") {
            $result = array(
                "status" => "404",
                "error" => "User does not exist"
            );
            return $result;
        } else {
            return $this->RequestUserToken($username);
        }
    }

    public function SuspendUser($username) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $dataArray = array(
            'Method' => "SU",
            'Timestamp' => "$timestamp",
            'Username' => "$username"
        );

        $signature = $this->signature_gen($this->secret, $dataArray);


        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
            'form_params' => array(
                //   'AppID' => "TF39",
                //     "Signature" => "qpdcrnoddr7m6",
                'Method' => 'SU',
                'Timestamp' => $timestamp,
                'Username' => $username
        ));

        $res = $client->post(
                $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );


        //var_dump($res);
        //echo $res->getStatusCode();


        $status_code = $res->getStatusCode();


        // if user exists 
        if ($status_code == "200") {
            $data = json_decode($res->getBody(), true);

            $result = array("success" => true);

            return $result;
        } else if ($status_code == "404") {
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        } else {
            $result = array(
                "error" => "Error"
            );
            return $result;
        }
    }

    public function SetStatusUser($username, $status) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $dataArray = array(
            'Method' => 'SS',
            'Status' => $status,
            'Timestamp' => $timestamp,
            'Username' => $username
        );

        $signature = $this->signature_gen($this->secret, $dataArray);


        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
            'form_params' => array(
                //   'AppID' => "TF39",
                //     "Signature" => "qpdcrnoddr7m6",
                'Method' => 'SS',
                'Status' => $status,
                'Timestamp' => $timestamp,
                'Username' => $username
        ));
        $res = $client->post(
                $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );


        $status_code = $res->getStatusCode();



        // if user exists 
        if ($status_code == "200") {

            $result = array("success" => true);
            return $result;
        } else if ($status == "404") {
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        } else {
            $result = array(
                "error" => "Error"
            );
            return $result;
        }
    }

    public function SetPassword($username, $password) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $dataArray = array(
            'Method' => 'SP',
            'Password' => $password,
            'Timestamp' => $timestamp,
            'Username' => $username
        );

        $signature = $this->signature_gen($this->secret, $dataArray);


        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
            'form_params' => array(
                //   'AppID' => "TF39",
                //     "Signature" => "qpdcrnoddr7m6",
                'Method' => 'SP',
                'Password' => $password,
                'Timestamp' => $timestamp,
                'Username' => $username
        ));

        $res = $client->post(
                $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );


        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(), true);

        // if user exists 
        if ($status_code == "200") {

            $result = array(
                "success" => true,
                "username" => "TF39.".$username,
                "password" => $password
            );

            return $result;
        } else if ($status_code == "404") {

            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        } else {

            return $this->SetPassword($username, $password);
        }
    }

    public function ForwardingToTheGameSite($token) {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $dataArray = array(
            "token" => $token
        );

//        $dataReq = array();
//        if (null != $RequestID) {
//            $dataReq = array(
//                'game' => $game
//            );
//        }
        //$dataArray = array_merge($dataArray, $dataReq);
        //$signature = $this->signature_gen($this->secret,$dataArray);


        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
            'form_params' => array(
                //   'AppID' => "TF39",
                //     "Signature" => "qpdcrnoddr7m6",
                "token" => $token
        ));

        $res = $client->post(
                $this->url_forward, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );


        $status_code = $res->getStatusCode();
//        echo $this->url_forward."<br>";
//        var_dump($post_data);
        //echo $res->getBody();
        return $res->getBody();

        // if ($status_code == "200") {
        // $re = $res->getBody();
        // if ($res->getBody() == "Bad Request: Session was not found") {
        // http_response_code(503);
        // echo json_encode("Service Unavailable");
        // } else {
        // echo $res->getBody();
        // }
        // } else {
        // $result = array(
        // "error" => "Error"
        // );
        // echo json_encode($result);
        // }
    }

   public function joker_log($funcName, $state, $details, $game) {
        $date = date('d-m-Y');
        $logfile = 'D:/log/joker/Log_Joker_TRANSFER' . $date . '.log';
        $logger = new Logger('Joker_log');
// Now add some handlers
        $logger->pushHandler(new StreamHandler($logfile, Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());

        $msg = "[$funcName] [$game] Details : { $details }";
// You can now use your logger

        switch ($state) {
            case "info":
                $logger->info($msg . "\n");
                break;
            case "debug":
                $logger->debug($msg . "\n");
                break;
            case "alert":
                $logger->alert($msg . "\n");
                break;
            case "critical":
                $logger->critical($msg . "\n");
                break;
            case "emergency":
                $logger->EMERGENCY($msg . "\n");
                break;
            case "error":
                $logger->ERROR($msg . "\n");
                break;
            default:
        }
    }
    
    

}

?>