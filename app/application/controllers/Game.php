<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;

//use Wrapper\JokerApiWrapper;
require 'vendor/autoload.php';

class game extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('customautoloader');
    }

    public function index() {
        $this->load->view('welcome_message');
    }

    public function getList() {
        $token = $this->input->post("token");

        echo json_encode(array("success" => true));
    }

    public function getInfo() {
        $token = $this->input->post("token");

        $start_month = "";
        $end_month = "";

        if (null != $this->input->post("start_month")) {
            $start_month = $this->input->post("start_month");
        }

        if (null != $this->input->post("end_month")) {
            $end_month = $this->input->post("end_month");
        }



        echo json_encode(array("success" => true));
    }

    public function matchResult() {
        $token = $this->input->post("token");
        $game_id = $this->input->post("game_id");
        $arena = $this->input->post("arena");
        $date = "";

        if (null != $this->input->post("date")) {
            $date = $this->input->post("date");
        }

        echo json_encode(array("success" => true));
    }

    public function forwardToGame() {
        //header('Content-Type: application/json');

        $token = $this->input->post("token");
        //$gamecode = $this->input->post("game");
        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->ForwardingToTheGameSite($token);

        if ($result == "Bad Request: Session was not found") {
            http_response_code(503);
            //echo json_encode("service unavailable");
        } else {
            echo $result;
        }
        
    }

}
