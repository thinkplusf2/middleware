<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

//use Wrapper\JokerApiWrapper;

require 'vendor/autoload.php';

class cockfight extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('customautoloader');
        $this->load->model('single_wallet_model');
        $this->load->model('console_model');
    }

    var $cert = "hi5BDE7RJgIzYzZ9";

    public function test() {
        $client = new \GuzzleHttp\Client();
        $response = $client->put('http://httpbin.org/put');

        echo $response->getBody();
    }

    public function test_db() {
        $this->single_wallet_model->test();
    }

    public function index() {

        echo "Welcome";

        // var_dump($str);
    }

    public function GetKey_con() {
        // header('Content-Type: application/json');
        $cert = $this->cert;
        $user = $this->input->post("user");
        $agent = $this->input->post("agent");
        $balance = "0";
        //$balance = $this->input->post("balance");
        // พี่ตอยให้ balance fix ไว้เป็น 0 เพื่อให้ เป็นการ create user กรณียังไม่มี account
        // $currency = $this->input->post("currency");
        $currency = "4";
        $maxbet = $this->input->post("maxbet");
        $minbet = $this->input->post("minbet");
        $matchlimit = $this->input->post("matchlimit");
        $mindraw = $this->input->post("mindraw");
        $maxdraw = $this->input->post("maxdraw");

        $dataArray = array(
            "cert" => $cert,
            "user" => $user,
            "agent" => $agent,
            "balance" => $balance,
            "currency" => $currency,
            "maxbet" => $maxbet,
            "minbet" => $minbet,
            "matchlimit" => $matchlimit,
            "mindraw" => $mindraw,
            "maxdraw" => $maxdraw
        );
        $api = new Wrapper\CockFightApiWrapper();

        $result = $api->GetKey($dataArray);
        //var_dump($result);
        echo json_encode($result);
    }

    public function Login_con() {
        //header('Content-Type: application/json');
        $user = $this->input->post("user");
        $key = $this->input->post("key");
        $balance = $this->input->post("balance");
        $language = $this->input->post("language");
        $extension1 = $this->input->post("extension1");
        $extension2 = $this->input->post("extension2");
        $extension3 = $this->input->post("extension3");
        // if(null!=$this->input->post("user")){
        //     $user = $this->input->post("user");
        // }
        // else{
        //     $user = "";
        // }
        // if(null!=$this->input->post("key")){
        //     $key = $this->input->post("key");
        // }
        // else{
        //     $key = "";
        // }
        // if(null!=$this->input->post("balance")){
        //     $balance = $this->input->post("balance");
        // }
        // else{
        //     $balance = "";
        // }
        // if(null!=$this->input->post("language")){
        //     $language = $this->input->post("language");
        // }
        // else{
        //     $language = "";
        // }
        // if(null!=$this->input->post("extension1")){
        //     $extension1 = $this->input->post("extension1");
        // }
        // else{
        //     $extension1 = "";
        // }
        // if(null!=$this->input->post("extension2")){
        //     $extension2 = $this->input->post("extension2");
        // }
        // else{
        //     $extension2 = "";
        // }
        // if(null!=$this->input->post("extension3")){
        //     $extension3 = $this->input->post("extension3");
        // }
        // else{
        //     $extension3 = "";
        // }
        // $logoutUrl = "";
        // $isDemo ="";
        $dataArray = array(
            "user" => $user,
            "key" => $key,
            "balance" => $balance,
            "language" => $language,
            "extension1" => $extension1,
            "extension2" => $extension2,
            "extension3" => $extension3
                ,
                // ,"logoutUrl" => $logoutUrl,
                // "isDemo" => $isDemo
        );

        $api = new Wrapper\CockFightApiWrapper();
        //var_dump("hey");
        $result = $api->Login($dataArray);

        echo $result;
    }

    public function MobileLogin_con() {

        $user = $this->input->post("user");
        $key = $this->input->post("key");
        $language = $this->input->post("language");


        $dataArray = array(
            "user" => $user,
            "key" => $key,
            "language" => $language
        );

        $api = new Wrapper\CockFightApiWrapper();

        $result = $api->MobileLogin($dataArray);
    }

    public function Logout_con() {
        header('Content-Type: application/json');
        $cert = $this->cert;
        $user = $this->input->post("user");

        $dataArray = array(
            "cert" => $cert,
            "user" => $user
        );

        $api = new Wrapper\CockFightApiWrapper();


        $result = $api->Logout($dataArray);
        echo json_encode($result);
    }

    public function UpdateBetSettings_con() {
        header('Content-Type: application/json');

        $cert = $this->cert;
        $user = $this->input->post("user");
        $maxbet = $this->input->post("maxbet");
        $minbet = $this->input->post("minbet");
        $matchlimit = $this->input->post("matchlimit");
        $mindraw = $this->input->post("mindraw");
        $maxdraw = $this->input->post("maxdraw");

        $dataArray = array(
            "cert" => $cert,
            "user" => $user,
            "maxbet" => $maxbet,
            "minbet" => $minbet,
            "matchlimit" => $matchlimit,
            "mindraw" => $mindraw,
            "maxdraw" => $maxdraw
        );
        //var_dump($dataArray);

        $api = new Wrapper\CockFightApiWrapper();

        $result = $api->UpdateBetSettings($dataArray);

        echo json_encode($result);
    }

    public function GetMatchResults_con() {
        header('Content-Type: application/json');
        $cert = $this->cert;
        $date = $this->input->post("date");
        $arena = $this->input->post("arena");

        $dataArray = array(
            "cert" => $cert,
            "date" => $date,
            "arena" => $arena
        );

        // var_dump($dataArray);

        $api = new Wrapper\CockFightApiWrapper();

        $result = $api->GetMatchResults($dataArray);
        echo json_encode($result);
    }

    public function GetSchedule_con() {
        header('Content-Type: application/json');

        $cert = $this->cert;
        $startmonth = $this->input->post("startmonth");
        $endmonth = $this->input->post("endmonth");

        $dataArray = array(
            "cert" => $cert,
            "startmonth" => $startmonth,
            "endmonth" => $endmonth
        );

        //var_dump($dataArray);

        $api = new Wrapper\CockFightApiWrapper();

        $result = $api->GetSchedule($dataArray);
        echo json_encode($result);
    }

    public function PlaceBet_con() {
        header('Content-Type: application/json');
        $api = new Wrapper\CockFightApiWrapper();
//        $action = "bet";
//        $txId = $this->input->post("txId");
//        $userId = $this->input->post("userId");
//        $betAmount = $this->input->post("betAmount");
//
//        $betTime = $this->input->post("betTime");
//        $updateTime = $this->input->post("updateTime");
//        $eventDate = $this->input->post("eventDate");
//        $choice = $this->input->post("choice");
//        $odds = $this->input->post("odds");
//        $url = $this->input->post("url");
//        $api = new Wrapper\CockFightApiWrapper();
//        $realBetAmount = $api->CalRealAmount($betAmount, $this->input->post("odds"));

        $data = json_decode(file_get_contents('php://input'));

        //var_dump($data->transactions[0]);

        $action = $data->action;



        foreach ($data->transactions as $row) {
            $txId = $row->txId;
            $userId = $row->userId;
            $betAmount = $row->betAmount;

            $betTime = $row->betTime;
            $updateTime = $row->updateTime;
            $eventDate = $row->eventDate;
            $choice = $row->choice;
            $odds = $row->odds;


            $realBetAmount = $api->CalRealAmount($betAmount, $odds);

            $array = array(
                'action' => $action,
                'txId' => $txId,
                'userId' => $userId,
                'betAmount' => $betAmount,
                'betTime' => $betTime,
                'updateTime' => $updateTime,
                'eventDate' => $eventDate,
                'choice' => $choice,
                'odds' => $odds,
                'realBetAmount' => $realBetAmount
            );

            $res = $this->single_wallet_model->placeBet($array);

            echo json_encode($res);
        }



        //var_dump($count);
//        
//        $ArData = array(
//            "txId" => $txId,
//            "userId" => $userId,
//            "betAmount" => $betAmount,
//            "updateDate" => $updateTime,
//            "betTime" => $betTime,
//            "eventDate" => $eventDate,
//            "choice" => $choice,
//            "odds" => $odds,
//            "betAmount" => $betAmount,
//            "realBetAmount" => $realBetAmount,
//            "url" => $url
//        );
//
//        if ($odds < 0) {
//
//            //$this->UpdateBet_con($ArData);
//        }
//
//        $data = array(
//            "action" => $action, "transactions"
//            => array(
//                "txId" => $txId, "userId" => $userId, "betAmount" => $betAmount, "realBetAmount" => $realBetAmount,
//                "betTime" => $betTime, "updateTime" => $updateTime, "eventDate" => $eventDate, "choice" => $choice, "odds" => $odds
//            )
//        );
//        echo json_encode($data);
//        echo "<br>";
        //var_dump(json_encode($data));
//        $result = $api->PlaceBet($data, $url);
//
//        if ($result['status'] == "500") {
//            $this->CancelBet_con($ArData);
//        } else if ($result['status'] == "200") {
//
//            //echo json_encode($result);
//            echo json_encode(array("success"=> true));
//        }
        //echo json_encode(array("success" => true));
    }

    public function CancelBet_con() {
        header('Content-Type: application/json');
        $api = new Wrapper\CockFightApiWrapper();
//        $action = "cancelBet";
//        $txId = $this->input->post("txId");
//        $userId = $this->input->post("userId");
//        $betAmount = $this->input->post("betAmount");
//
//        $betTime = $this->input->post("betTime");
//        $updateTime = $this->input->post("updateTime");
//        $eventDate = $this->input->post("eventDate");
//        $choice = $this->input->post("choice");
//        $odds = $this->input->post("odds");
//        $url = $this->input->post("url");
//        $realBetAmount = $api->CalRealAmount($betAmount, $this->input->post("odds"));


        $data = json_decode(file_get_contents('php://input'));

        //var_dump($data->transactions[0]);

        $action = $data->action;



        foreach ($data->transactions as $row) {

            $txId = $row->txId;
            $userId = $row->userId;
            $betAmount = $row->betAmount;

            $betTime = $row->betTime;
            $updateTime = $row->updateTime;
            $eventDate = $row->eventDate;
            $choice = $row->choice;
            $odds = $row->odds;

            $realBetAmount = $row->realBetAmount;

            $array = array(
                'action' => $action,
                'txId' => $txId,
                'userId' => $userId,
                'betAmount' => $betAmount,
                'betTime' => $betTime,
                'updateTime' => $updateTime,
                'eventDate' => $eventDate,
                'choice' => $choice,
                'odds' => $odds,
                'realBetAmount' => $realBetAmount
            );

            $res = $this->single_wallet_model->cancelBet($array);
        }


        // =>[ $ArrayData]];
//        $data = array(
//            "action" => $action, "transactions"
//            => array(
//                "txId" => $txId, "userId" => $userId, "betAmount" => $betAmount, "realBetAmount" => $realBetAmount,
//                "betTime" => $betTime, "updateTime" => $updateTime, "eventDate" => $eventDate, "choice" => $choice, "odds" => $odds//
//            )
//        );
        //echo json_encode($data);
        //echo "<br>";
        //var_dump(json_encode($data));
        // $result = $api->CancelBet($data, $url);
        //echo json_encode($result);
        echo json_encode($res);
    }

    public function UpdateBet_con() {
        header('Content-Type: application/json');
        $api = new Wrapper\CockFightApiWrapper();

//        $action = "adjust";
//        $txId = $this->input->post("txTd");
//        $userId = $this->input->post("userId");
//        $adjustAmount = $this->input->post("adjustAmount");
//        $betTime = $this->input->post("betTime");
//        $betAmount = $this->input->post("betAmount");
//        $realBetAmount = $this->input->post("realBetAmount");
//        $updateTime = $this->input->post("updateTime");
//        $odds = $this->input->post("odds");
//
//        $adjustAmount = $betAmount - ($odds * $betAmount);

        $data = json_decode(file_get_contents('php://input'));

        //var_dump($data->transactions[0]);

        $action = $data->action;



        foreach ($data->transactions as $row) {

            $txId = $row->txId;
            $userId = $row->userId;
            $adjustAmount = $row->adjustAmount;
            $betTime = $row->betTime;

            $realBetAmount = $row->realBetAmount;
            $updateTime = $row->updateTime;


            $adjustAmount = $row->adjustAmount;

            $array = array(
                'action' => $action,
                'txId' => $txId,
                'userId' => $userId,
                'adjustAmount' => $adjustAmount,
                'betTime' => $betTime,
                'realBetAmount' => $realBetAmount,
                'updateTime' => $updateTime,
            );

            $res = $this->single_wallet_model->updateBet($array);
        }


        //$url = $this->input->post("url");
        //$realBetAmount = $this->CalRealAmount($betAmount, $this->input->post("odds"));
        // $ArrayData = array(
        //     "txId"=>$txId , "userId" => $userId, "betAmount" => $betAmount,"realBetAmount"=>$realBetAmount,
        //     "betTime" => $betTime, "updateTime" => $updateTime, "eventDate"=>$eventDate
        // );
        // $data = ["action" => $action, "transactions" 
        // =>[ $ArrayData]];
//        $data = array(
//            "action" => $action, "transactions"
//            => array(
//                "txId" => $txId, "userId" => $userId, "realBetAmount" => $realBetAmount, "adjustAmount" => $adjustAmount,
//                "betTime" => $betTime, "updateTime" => $updateTime
//            )
//        );
        //echo json_encode($data);
        // $result = $api->UpdateBet($data, $url);
        //echo json_encode($result);
        echo json_encode($res);
    }

    public function SettleMatch_con() {
        header('Content-Type: application/json');

        $api = new Wrapper\CockFightApiWrapper();

//        $action = $this->input->post("action");
//        $userId = $this->input->post("userId");
//        $txId = $this->input->post("txId");
//        $betTime = $this->input->post("betTime");
//        $eventDate = $this->input->post("eventDate");
//        $updateTime = $this->input->post("updateTime");
//        $arena = $this->input->post("arena");
//        $matchno = $this->input->post("matchno");
//        $choice = $this->input->post("choice");
//        $odds = $this->input->post("odds");
//        $betAmount = $this->input->post("betAmount");
//        $status = $this->input->post("status");
//        $result = $this->input->post("result");
//        $settled = $this->input->post("settled");
//        $commission = $this->input->post("commission");
//
//        $url = $this->input->post("url");



        $data = json_decode(file_get_contents('php://input'));


//        $datenow = date("dmy-hhmmii") . "";
//        $txt = json_encode($data);
//        $myfile = fopen("D:/log/$datenow".".txt", "w") or die("Unable to open file!");
//
//        fwrite($myfile, $txt);
//        fclose($myfile);
        //var_dump($data->transactions[0]);

        $action = $data->action;



        foreach ($data->transactions as $row) {


            $userId = $row->userId;
            $txId = $row->txId;
            $betTime = $row->betTime;
            $eventDate = $row->eventDate;
            $updateTime = $row->updateTime;
            $arena = $row->arena;
            $matchno = $row->matchno;
            $choice = $row->choice;
            $odds = $row->odds;
            $betAmount = $row->betAmount;
            $status = $row->status;
            $result = $row->result;
            $settled = $row->settled;
            $commission = $row->commission;
            $arrayWin = array(
                "betAmount" => $betAmount,
                "status" => $status,
                "odds" => $odds
            );
            // $winLoss = $api->WinLossCal($arrayWin);
            $winLoss = $row->winLoss;
            // if win,loss,draw


            $array = array(
                "userId" => $userId,
                "txId" => $txId,
                "betTime" => $betTime,
                "eventDate" => $eventDate,
                "updateTime" => $updateTime,
                "arena" => $arena,
                "matchno" => $matchno,
                "choice" => $choice,
                "odds" => $odds,
                "betAmount" => $betAmount,
                "status" => $status,
                "result" => $result,
                "settled" => $settled,
                "commission" => $commission,
                "winLoss" => $winLoss
            );
            $cal = $this->single_wallet_model->Calculate_winloss($array);
            $res = $this->single_wallet_model->settleMatch($array);

            
        }

//        $winLoss = $api->WinLossCal($arrayWin);
//
//        $array = array(
//            "action" => $action,
//            "transactions" => array(
//                "userId" => $userId,
//                "txId" => $txId,
//                "betTime" => $betTime,
//                "eventDate" => $eventDate,
//                "updateTime" => $updateTime,
//                "arena" => $arena,
//                "matchno" => $matchno,
//                "choice" => $choice,
//                "odds" => $odds,
//                "beAmount" => $betAmount,
//                "status" => $status,
//                "result" => $result,
//                "settled" => $settled,
//                "winLoss" => $winLoss,
//                "commission" => $commission
//            )
//        );
        // $result = $api->SettleMatch($array, $url);
        //echo json_encode($result);
        echo json_encode($cal);
    }

    public function VoidLateClose_con() {
        header('Content-Type: application/json');

//        $action = $this->input->post("action");
//        $txId = $this->input->post("txId");
//        $updateTime = $this->input->post("updateTime");
//        $url = $this->input->post("url");
//
//        $data = array(
//            "action" => $action,
//            "transactions" => array(
//                "txId" => $txId,
//                "updateTime" => $updateTime
//            )
//        );

        $api = new Wrapper\CockFightApiWrapper();
        $data = json_decode(file_get_contents('php://input'));

        //var_dump($data->transactions[0]);

        $action = $data->action;



        foreach ($data->transactions as $row) {

            $txId = $row->txId;
            $updateTime = $row->updateTime;

            $array = array(
                "action" => $action,
                "txId" => $txId,
                "updateTime" => $updateTime
                
            );
            $res = $this->single_wallet_model->voidLateClose($array);
        }

        //$result = $api->VoidLateClose($data, $url);
        //echo json_encode($result);
        echo json_encode($res);
    }

    public function Refund_con() {
        header('Content-Type: application/json');

//        $action = "refund";
//        $txId = $this->input->post("txId");
//        $betTime = $this->input->post("betTime");
//        $eventDate = $this->input->post("eventDate");
//        $updateTime = $this->input->post("updateTime");
//        $arena = $this->input->post("arena");
//        $arenaCode = $this->input->post("arenaCode");
//        $location = $this->input->post("location");
//        $matchno = $this->input->post("matchno");
//        $matchResult = $this->input->post("matchResult");
//        $choice = $this->input->post("choice");
//        $odds = $this->input->post("odds");
//        $betAmount = $this->input->post("betAmount");
//        $status = $this->input->post("status");
//        $settled = $this->input->post("settled");
//        $live = $this->input->post("live");
        $winLoss = "";

        //$url = $this->input->post("url");


        $api = new Wrapper\CockFightApiWrapper();



//        $data = array(
//            "action" => $action,
//            "transactions" => array(
//                "txId" => $txId,
//                "betTime" => $betTime,
//                "eventDate" => $eventDate,
//                "updateTime" => $updateTime,
//                "arena" => $arena,
//                "arenaCode" => $arenaCode,
//                "location" => $location,
//                "matchno" => $matchno,
//                "matchResult" => $matchResult,
//                "choice" => $choice,
//                "odds" => $odds,
//                "betAmount" => $betAmount,
//                "status" => $status,
//                "settled" => $settled,
//                "winLoss" => $winLoss,
//                "live" => $live
//            )
//        );

        $data = json_decode(file_get_contents('php://input'));



        $action = $data->action;

//var_dump($data->transactions[0]);

        foreach ($data->transactions as $row) {

            $txId = $row->txId;
            $betTime = $row->betTime;
            $eventDate = $row->eventDate;
            $updateTime = $row->updateTime;
            $arena = $row->arena;
            $arenaCode = $row->arenaCode;
            $location = $row->location;
            $matchno = $row->matchno;
            $matchResult = $row->matchResult;
            $choice = $row->choice;
            $odds = $row->odds;
            $betAmount = $row->betAmount;
            $status = $row->status;
            $settled = $row->settled;
            $live = $row->live;
            $arrayWin = array(
                "betAmount" => $betAmount,
                "status" => $status,
                "odds" => $odds
            );
            $winLoss = $api->WinLossCal($arrayWin);


            $array = array(
                "txId" => $row->txId,
                "betTime" => $row->betTime,
                "eventDate" => $row->eventDate,
                "updateTime" => $row->updateTime,
                "arena" => $row->arena,
                "arenaCode" => $row->arenaCode,
                "location" => $row->location,
                "matchno" => $row->matchno,
                "matchResult" => $row->matchResult,
                "choice" => $row->choice,
                "odds" => $row->odds,
                "betAmount" => $row->betAmount,
                "status" => $row->status,
                "settled" => $row->settled,
                "live" => $row->live,
                "winLoss" => $winLoss
            );
            $res = $this->single_wallet_model->refund($array);
        }

        //$result = $api->Refund($data, $url);
        //echo json_encode($result);
        echo json_encode($res);
    }

    public function Create_con() {
        header('Content-Type: application/json');
        $cert = $this->cert;
        $user = $this->input->post("user");
        $agent = $this->input->post("agent");
        $balance = "0";
        // $currency = $this->input->post("currency");
        $currency = "4";
        $maxbet = $this->input->post("maxbet");
        $minbet = $this->input->post("minbet");
        $matchlimit = $this->input->post("matchlimit");
        $mindraw = $this->input->post("mindraw");
        $maxdraw = $this->input->post("maxdraw");

        $dataArray = array(
            "cert" => $cert,
            "user" => $user,
            "agent" => $agent,
            "balance" => $balance,
            "currency" => $currency,
            "maxbet" => $maxbet,
            "minbet" => $minbet,
            "matchlimit" => $matchlimit,
            "mindraw" => $mindraw,
            "maxdraw" => $maxdraw
        );
        $api = new Wrapper\CockFightApiWrapper();

        $result = $api->GetKey($dataArray);
        //var_dump($result);
        echo json_encode($result);


        //echo json_encode(array("success"=> true));
    }

    public function Update_con() {
        header('Content-Type: application/json');
        $username = $this->input->post("Username");

        echo json_encode(array("success" => true));
    }

    public function ChangePassword_con() {
        header('Content-Type: application/json');
        $username = $this->input->post("Username");
        $password = $this->input->post("Password");


        echo json_encode(array("success" => true));
    }

    public function Suspend_con() {
        header('Content-Type: application/json');
        $username = $this->input->post("Username");



        echo json_encode(array("success" => true));
    }

    public function GetBalance_con() {
        header('Content-Type: application/json');



        $username = $this->input->post("username");

        $type = $this->input->post("type");

        $array = array(
            "username" => $username,
            "type" => $type
        );

        $res = $this->single_wallet_model->get_credit($array);

        echo json_encode($res);
    }

    public function GetBalance_con2() {
        header('Content-Type: application/json');


        $data = json_decode(file_get_contents('php://input'));

        foreach ($data as $row) {
            $username = $data->userID;

            $type = "cockfight";

            $array = array(
                "username" => $username,
                "type" => $type
            );
            
            $res = $this->single_wallet_model->get_credit($array);
        }


        echo json_encode($res);
    }

    public function Deposit_con() {
        header('Content-Type: application/json');
        $username = $this->input->post("Username");
        $amount = $this->input->post("Amount");


        echo json_encode(array("success" => true));
    }

    public function Withdraw_con() {
        header('Content-Type: application/json');
        $username = $this->input->post("Username");
        $amount = $this->input->post("Amount");


        echo json_encode(array("success" => true));
    }

}
