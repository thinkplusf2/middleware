<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

//use Wrapper\JokerApiWrapper;


require 'vendor/autoload.php';

class user extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->library('customautoloader');
        //$this->load->model('user_model', '', true);    
        $this->load->model('single_wallet_model');
        $this->load->model('joker_model');
    }

    var $cert = "hi5BDE7RJgIzYzZ9";

    public function index() {
        echo "home";
//        $this->load->view('templates/header');
//        $this->load->view('home');
//        $this->load->view('templates/footer');
    }

    public function register() {
        $name = $this->input->post("name");

        $max_bet = "";
        $min_bet = "";

        if (null != $this->input->post("max_bet")) {
            $max_bet = $this->input->post("max_bet");
        }

        if (null != $this->input->post("min_bet")) {
            $min_bet = $this->input->post("min_bet");
        }

        $currency = $this->input->post("currency");
        $laguage = "";

        if (null != $this->input->post("language")) {
            $language = $this->input->post("language");
        }



        echo json_encode(array("success" => true));
    }

    public function login() {

        $user = $this->input->post("user");
        $pass = $this->input->post("pass");

        if (null != $user && null != $pass) {
            echo json_encode(array("success" => true));
        } else {
            echo json_encode(array("success" => false));
        }
    }

    public function logout() {

        $token = $this->input->post("token");
        echo json_encode(array("success" => true));
    }

    public function getInfo() {
        $token = $this->input->post("token");



        echo json_encode(array("success" => true));
    }

    public function create_joker() {
        header('Content-Type: application/json');
        $username = $this->input->post("username");
        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->EnsureUserAccount($username);

        $cert = $this->cert;
        $user = $this->input->post("username");
        $agent = $this->input->post("agent");
        $balance = "0";
        // $currency = $this->input->post("currency");
        $currency = "4";
        $maxbet = $this->input->post("maxbet");
        $minbet = $this->input->post("minbet");
        $matchlimit = $this->input->post("matchlimit");
        $mindraw = $this->input->post("mindraw");
        $maxdraw = $this->input->post("maxdraw");

        $dataArray = array(
            "cert" => $cert,
            "user" => $user,
            "agent" => $agent,
            "balance" => $balance,
            "currency" => $currency,
            "maxbet" => $maxbet,
            "minbet" => $minbet,
            "matchlimit" => $matchlimit,
            "mindraw" => $mindraw,
            "maxdraw" => $maxdraw
        );
        $api = new Wrapper\CockFightApiWrapper();

        //$ret = $api->GetKey($dataArray);

        $return = $this->single_wallet_model->create_user($dataArray);

        $arr_res = array(
            "message" => $return
        );



        echo json_encode($arr_res);
    }

    public function create_users() {
        header('Content-Type: application/json');
        $username = $this->input->post("username");
        $dataArray = array(
            "user" => $username
        );
        $return = $this->single_wallet_model->create_user($dataArray);
        
        echo json_encode($return);
    }

    public function create_cock() {
        header('Content-Type: application/json');
        $username = $this->input->post("username");
        //$api = new JokerApiWrapper();
        $result = array();
//        $api = new Wrapper\JokerApiWrapper();
//
//        $result = $api->EnsureUserAccount($username);

        $cert = $this->cert;
        $user = $this->input->post("username");
        $agent = $this->input->post("agent");
        $balance = "0";
        // $currency = $this->input->post("currency");
        $currency = "4";
        $maxbet = $this->input->post("maxbet");
        $minbet = $this->input->post("minbet");
        $matchlimit = $this->input->post("matchlimit");
        $mindraw = $this->input->post("mindraw");
        $maxdraw = $this->input->post("maxdraw");

        $dataArray = array(
            "cert" => $cert,
            "user" => $user,
            "agent" => $agent,
            "balance" => $balance,
            "currency" => $currency,
            "maxbet" => $maxbet,
            "minbet" => $minbet,
            "matchlimit" => $matchlimit,
            "mindraw" => $mindraw,
            "maxdraw" => $maxdraw
        );
        $api = new Wrapper\CockFightApiWrapper();

        $ret = $api->GetKey($dataArray);

        $return = $this->single_wallet_model->create_user($dataArray);

        $arr_res = array(
            "message" => $return
        );

//        \Ratchet\Client\connect('ws://54.255.196.238:8080/LNXMVMainApp/fmxlnxhdpboardendpoint')->then(function($conn) {
//            $conn->on('message', function($msg) use ($conn) {
//                echo "Received: {$msg}\n";
//                $conn->close();
//            });
//
//            $conn->send('{"sender":"jkrwatcher","action":"cnswatcher_alert_in"}');
//        }, function ($e) {
//            echo "Could not connect: {$e->getMessage()}\n";
//        });

        echo json_encode($arr_res);
    }

    public function RequestUserToken_con() {
        header('Content-Type: application/json');

        $username = $this->input->post("Username");
        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->RequestUserToken($username);
        //$result = json_encode($result);

        if ($result == "null")
            http_response_code(503);
        else
            echo json_encode($result);
    }

    public function RequestToken_redirect() {
        header('Content-Type: application/json');

        $username = $this->input->post("username");
        $gamecode = $this->input->post("game");

        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->RequestUserToken($username);

//        $token = $result['Token'];
//        
//        $re = $api->ForwardingToTheGameSite($token,$gamecode);  
//        if($re == "no"){
//            http_response_code(503);
//            echo json_encode("Service Unavailable");
//        }else{
//            echo json_encode($re);
//        }

        echo json_encode($result);

//        echo json_encode($result);
    }

    public function suspend() {
        header('Content-Type: application/json');

        $username = $this->input->post("Username");
        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->SuspendUser($username);
        echo json_encode($result);
    }

    public function setStatus() {
        header('Content-Type: application/json');

        $username = $this->input->post("Username");
        $status = $this->input->post("Status");
        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->SetStatusUser($username, $status);
        echo json_encode($result);
    }

    public function changePassword() {
        header('Content-Type: application/json');

        $username = $this->input->post("Username");
        $password = $this->input->post("Password");
        //$api = new JokerApiWrapper();
        $result = array();

        $this->joker_model->savePassword($username, $password);

        $api = new Wrapper\JokerApiWrapper();

        $result = $api->SetPassword($username, $password);
        echo json_encode($result);
    }

    public function setUpPassword() {
        header('Content-Type: application/json');

        $username = $this->input->post("Username");
        $password = $this->input->post("Password");

        $result = array();

        $this->joker_model->savePassword($username, $password);

        $api = new Wrapper\JokerApiWrapper();

        $result = $api->SetPassword($username, $password);
        echo json_encode($result);
    }

    public function delete_cock() {
        header('Content-Type: application/json');

        $username = $this->input->post("username");

        $res = $this->single_wallet_model->delete_user($username);

        echo json_encode($res);
    }

}
