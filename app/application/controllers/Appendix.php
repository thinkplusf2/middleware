<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use GuzzleHttp\Client;
//use Wrapper\JokerApiWrapper;
require 'vendor/autoload.php';
class appendix extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
    }
    
    public function currencyList(){
        $id = $this->input->post("id");
        $currency = $this->input->post("currency");
        $symbol = $this->input->post("symbol");

        echo json_encode(array("success"=>true));

    }

    public function statusCode(){
        $code = $this->input->post("code");
        $desc = $this->input->post("desc");       

        echo json_encode(array("success"=>true));
        
    }

    public function parameterMapping(){
        $secret_key = $this->input->post("secret_key");

        echo json_encode(array("success"=>true));
    }
}
