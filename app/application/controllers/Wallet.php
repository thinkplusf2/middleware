<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use \Monolog\Formatter\LineFormatter;

require 'vendor/autoload.php';

class wallet extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('customautoloader');
        $this->load->model('single_wallet_model');
        $this->load->model('joker_model');
    }

    public function index() {
        $this->load->view('welcome_message');
    }

    public function getBalances() {
        $token = $this->input->post("token");
        echo json_encode(array("success" => true));
    }

    public function transferin() {
        $this->load->view("curl");
    }

    public function deposits() {
        header('Content-Type: application/json');
        $username = $this->input->post("username");
        $amount = $this->input->post("amount");
        $type = $this->input->post("type");

        $array = array(
            "username" => $username,
            "balance" => $amount,
            "type" => $type
        );

        $res = $this->single_wallet_model->deposit($array);
//        echo "hey";
        //var_dump($res);
        if ($res == true) {
            echo json_encode(array("success" => true));
        } else {
            echo json_encode(array("success" => false));
        }
    }

//    public function withDraw() {
//        header('Content-Type: application/json');
//        $username = $this->input->post("username");
//        $type = $this->input->post("type");
//
//        $array = array(
//            "username" => $username,
//            "type" => $type
//        );
//
//        $res = $this->single_wallet_model->withdraw($array);
////        echo "hey";
//        //var_dump($res);
//        if ($res == true) {
//            echo json_encode(array("success" => true));
//        } else {
//            echo json_encode(array("success" => false));
//        }
//    }

    public function getBalance() {
        //header('Content-Type: application/json');

        $username = $this->input->post("Username");
        $type = "joker";
        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->GetCredit($username);
        if (isset($result['error'])) {
            $dataArray = array(
                "user" => $username
            );

            $res = $api->EnsureUserAccount($username);

            //$ret = $api->GetKey($dataArray);

            $return = $this->single_wallet_model->create_user($dataArray);
            $result = $api->GetCredit($username);
        }
        echo json_encode($result);
    }

    public function updateBalance() {
        header('Content-Type: application/json');

        $username = $this->input->post("username");
        $type = "joker";
        //$api = new JokerApiWrapper();
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->GetCredit($username);
        //var_dump($result);
        $this->single_wallet_model->update_balance($result);

        echo json_encode($result);
    }

    public function updateAllBalance() {
        header('Content-Type: application/json');
        ini_set('max_execution_time', 600);
        $time_start = microtime(true);
        //$username = $this->input->post("username");
        $type = "joker";
        //$api = new JokerApiWrapper();
        
        $api = new Wrapper\JokerApiWrapper();

        // $result = $api->GetCredit($username);
        //var_dump($result);
        $this->single_wallet_model->update_all_balance();
        

        
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        
        $result = array(
            "time" => $time
        );
        echo json_encode($result);
    }

    public function getForWithDraw($username) {
        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        $result = $api->GetCredit($username);
        return $result['Credit'];
    }

//ถอน
    public function deposit() {
        header('Content-Type: application/json');
        $RequestID = $this->input->post("RequestID");
        $username = $this->input->post("username");
        $amount = $this->input->post("amount");
        $type = $this->input->post("type");
        $type = strtolower($type);
        $array = array(
            "username" => $username,
            "balance" => $amount,
            "type" => $type
        );


        $this->joker_log("DEPOSIT", "info", json_encode($array), "Joker");


        $result = array();
        $api = new Wrapper\JokerApiWrapper();

        if ($type != "joker") {
            $res = $this->single_wallet_model->deposit($array);
            echo json_encode($res);
        } else {
            $res = $this->single_wallet_model->deposit($array);
            $result = $api->TransferCredit($amount, $RequestID, $username);
            echo json_encode($result);
        }
    }

    public function withdraw() {
        header('Content-Type: application/json');

        $api = new Wrapper\JokerApiWrapper();
        $RequestID = $this->input->post("RequestID");
        $username = $this->input->post("username");
        // $type = $this->input->post("type");
        //$amount = -($this->getForWithDraw($username));
        $amount = -($this->input->post('amount'));
        $type = $this->input->post("type");
        $type = strtolower($type);
        $array = array(
            "username" => $username,
            "type" => $type,
            "balance" => $amount
        );

        $this->joker_log("WITHDRAW", "info", json_encode($array), "Joker");
        $result = array();
        if ($type != "joker") {
            $res = $this->single_wallet_model->withdraw($array);
            echo json_encode($res);
        } else {
            $this->updateBalance($username);
    
            $res = $this->single_wallet_model->withdraw($array);
            $result = $api->TransferCredit($amount, $RequestID, $username);
            echo json_encode($result);
        }
    }

    public function deposit_console() {
        header('Content-Type: application/json');

        $RequestID = $this->input->post("RequestID");
        $username = $this->input->post("username");
        // $type = $this->input->post("type");
        $amount = $this->input->post("amount");
        $type = $this->input->post("type");

        if ($type == "joker") {
            $if = $this->joker_model->check_joker_account($username);
            if ($if == 0) {
                $api = new Wrapper\JokerApiWrapper();

                $result = $api->EnsureUserAccount($username);
                $dataArray = array(
                    "user" => $username
                );
                $this->single_wallet_model->create_user($dataArray);
            }
        }



        $array = array(
            "username" => $username,
            "type" => $type,
            "balance" => $amount
        );
        //var_dump($array);
        $res = $this->single_wallet_model->deposit($array);
        if ($type != "joker") {
            echo json_encode($res);
        } else {
            $api = new Wrapper\JokerApiWrapper();
            $result = $api->TransferCredit($amount, $RequestID, $username);
            //echo json_encode($result);
        }
        echo json_encode($res);
    }

    public function withdraw_console() {
        header('Content-Type: application/json');


        $RequestID = $this->input->post("RequestID");
        $username = $this->input->post("username");
        // $type = $this->input->post("type");
        $amount = -($this->input->post("amount"));
        $type = $this->input->post("type");

        $if = $this->joker_model->check_joker_account($username);
        if ($if == 0) {
            $api = new Wrapper\JokerApiWrapper();

            $result = $api->EnsureUserAccount($username);
            $dataArray = array(
                "user" => $username
            );
            $this->single_wallet_model->create_user($dataArray);
        }


        $array = array(
            "username" => $username,
            "type" => $type,
            "balance" => $amount
        );

        //var_dump($array);

        $res = $this->single_wallet_model->withdraw($array);
        if ($type != "joker") {
            echo json_encode($res);
        } else {
            $api = new Wrapper\JokerApiWrapper();
            $result = $api->TransferCredit($amount, $RequestID, $username);
            //echo json_encode($result);
        }

        echo json_encode($res);
    }

    public function test_log_joker() {
        $data = $this->input->post("data");
        $this->test_log($data);
        //$this->test_echo($data);
    }

}
