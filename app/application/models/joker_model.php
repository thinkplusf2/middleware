<?php

class joker_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model("joker_model");
// $db2 = $this->load->database('middleware_db', TRUE);
    }

    public function update_balance($array) {

        $where = array(
            "username" => $array['Username'],
            "type" => "joker"
        );
        $db2 = $this->load->database('middleware_db', TRUE);

        $db2->set(array("balance" => $array['Credit']));


        $db2->where($where);
        $db2->update('wallet');

        return 1;
    }

    public function savePassword($username, $password) {
        $db2 = $this->load->database('middleware_db', TRUE);
        $where = array(
            "username" => $username
        );
        $qu1 = $db2->select("*")->from("joker_user_info")->where($where)->get();

        if ($qu1->num_rows() > 0) {
            $db2->set(array("password" => $password));
            $db2->where($where);
            $db2->update('joker_user_info');
            $array = array(
                "status" => "update password"
            );
            return $array;
        } else {
            $data = array(
                "username" => $username,
                "password" => $password
            );
            $db2->insert("joker_user_info", $data);
            $array = array(
                "status" => "insert password"
            );
            return $array;
        }
    }

    public function check_joker_account($username) {
        $db2 = $this->load->database("middleware_db", true);

        $res = $db2->select("*")->from("wallet")->where(array(
                    "username" => $username,
                    "type" => "joker"
                ))->get();

        if ($res->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }


}
