<?php
namespace models;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use DateTime;
require 'vendor/autoload.php';
class JokerApiWrapper 
{
     protected $data = Array(); //protected variables goes here its declaration

    // var $url_joker = 'https://www.superbo68.net';
    var $url_joker = 'http://api.joker688.net:81/';
    var $secret =  "qpdcrnoddr7m6";
    var $url_forward = "http://www.joker688.net/";
    //var $url_joker = 'http://www.joker688.net';
    
    
    function __construct() {

        
        
    }

    public function signature_gen($secret,$key){
        //$secret = "qpdcrnoddr7m6"
        // $key must be array ;
        $text = "";
        ksort($key);
        while ($key_name = current($key)) {
           
                $text .= key($key).'='.$key_name.'&';
            
            next($key);
        }
        // again 

        $text = substr($text,0,-1); // อันนี้คือะไร ตัดตัว & สุดท้ายออกครับ ok
        // var_dump($text);
        // echo "<br>";
        // var_dump($secret);
        // echo "<br>";
        $signature = base64_encode(hash_hmac("SHA1",$text, $secret, true));
        // echo "<br>";
        // var_dump($signature);
        // echo "<br>";
        //$signature = base64_encode(hash_hmac("SHA1",$key, $secret));
         //return substr( $signature, 0, -2);
        return  $signature;

    }


    
    public function EnsureUserAccount($username){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
        
        // $api = new JokerApiWrapper();
        // $result = $api->ensureUser(asdf, asdf); 
       //$data = "Method=CU&Timestamp=".$timestamp."&Username=".$username;

        $dataArray  = array(
            'Method' => "CU",
        'Timestamp' => "$timestamp",
       'Username' => "$username"
        );

        $signature = $this->signature_gen($this->secret,$dataArray);

        $post_data = array(
                // 'AppID' => "TF39",
                // "Signature" => "qpdcrnoddr7m6",
          'form_params' => array(
            //   'AppID' => "TF39",
            //     "Signature" => "qpdcrnoddr7m6",
                'Method' => "CU",
                 'Timestamp' => "$timestamp",
                'Username' => "$username"  
        )
    );

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );

            

          $status_code = $res->getStatusCode();
        
      
         //echo $res->getBody();

        
       // var_dump($res);

        if($status_code=="200"){
            $data = json_decode($res->getBody(),true);
            $status_res = $data['Status'];
           $result = array(
            "Status" => $data['Status']
           );

           return $result;
           
        }else if($status_code=="201"){
            $result = array(
                "error" => "Created"
            );
            return $result;
        }else {
            $result = array(
                "error" => "Error"
            );
            return $result;
        }     
    }

    

    public function GetCredit($username){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();

        $dataArray  = array(
            'Method' => 'GC',
            'Timestamp' => $timestamp,
            'Username' => $username
        );

        $signature = $this->signature_gen($this->secret,$dataArray);
   

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
      'form_params' => array(
        //   'AppID' => "TF39",
        //     "Signature" => "qpdcrnoddr7m6",
        'Method' => 'GC',
        'Timestamp' => $timestamp,
        'Username' => $username
      ));

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
        // echo $res->getStatusCode();
        // //echo $res->getBody();
        // var_dump($res);
        // if user exists 
        if($status_code=="200"){
            $data = json_decode($res->getBody(),true);
            $username_res = $data['Username'];
            $credit_res = $data['Credit'];

            $result = array(
                "Username" => $username_res,
                "Credit" => $credit_res
            );

            return $result;

        }else if($status_code=="404"){
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        }else{
            $result = array(
                "error" => "Error"
            );
            return $result;
        }


    }

    public function TransferCredit($Amount,$RequestID,$username){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
       
        $dataArray  = array(
            'Amount' => "$Amount",
            'Method' => 'TC',
            
            'Timestamp' => $timestamp,
            'Username' => $username 
        );
        $dataReq = array();
        if(null!=$this->input->post("RequestID")){
            $dataReq = array(
                'RequestID' => $RequestID
            );
        }
           
        $dataArray = array_merge($dataArray,$dataReq);
        //var_dump($dataArray);
        $signature = $this->signature_gen($this->secret,$dataArray);
   

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
      'form_params' => array(
        'Amount' => $Amount,
        'Method' => 'TC',
        'RequestID' => $RequestID,
        'Timestamp' => $timestamp,
        'Username' => $username 
      ));

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );
    

        $status_code = $res->getStatusCode();


        // if user exists 
        if($status_code=="200"){
            $data = json_decode($res->getBody(),true);
            $result = array(
                "Username" => $data['Username'],
                "Credit" => $data['Credit'],
                "RequestID" => $data['RequestID'],
                "Time" => $data['Time'],
                "BeforeCredit" => $data['BeforeCredit']
            );

            return $result;
        }else if($status_code=="404"){
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        }else if($status_code=="400"){
            $result = array(
                "error" => "RequestID is already used or wrong format"
            );
            return $result;
        }
        
    }

    public function GetCreditTransfer($RequestID){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();

        $dataArray  = array(
            'Method' => 'TCH',
            'RequestID' => $RequestID,
            'Timestamp' => $timestamp
        );

        $signature = $this->signature_gen($this->secret,$dataArray);
   

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
      'form_params' => array(
        //   'AppID' => "TF39",
        //     "Signature" => "qpdcrnoddr7m6",
        'Method' => 'TCH',
        'RequestID' => $RequestID,
        'Timestamp' => $timestamp
      ));

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );

        $status_code = $res->getStatusCode();
        


        // if user exists 
        if($status_code=="200"){
            $data = json_decode($res->getBody(),true);
            
            $username_res = $data['Username'];
            $requestID_res = $data['RequestID'];
            $time_res = $data['Time'];
            $amount_res = $data['Amount'];
            

            $result = array(
                "Username" => $data['Username'],
                "Amount" => $data['Amount'],
                "RequestID" =>  $data['RequestID'],
                "Time" => $data['Time']
            );
            return $result;
        }else if($status_code=="404"){
            $result = array(
                "error" => "RequestID does not exist"
            );
            return $result;
        }else{
            $result = array(
                "error" => "Error"
            );
            return $result;
        }
    }



    public function RequestUserToken($username){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
        
        $dataArray  = array(
            'Method' => "RT",
        'Timestamp' => "$timestamp",
       'Username' => "$username"
        );

        $signature = $this->signature_gen($this->secret,$dataArray);
   

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
      'form_params' => array(
        //   'AppID' => "TF39",
        //     "Signature" => "qpdcrnoddr7m6",
        'Method' => 'RT',
        'Timestamp' => $timestamp,
        'Username' => $username  
      ));

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );

        $status_code = $res->getStatusCode();


        // if user exists 
        if($status_code=="200"){
            $data = json_decode($res->getBody(),true);
            
            $username_res = $data['Username'];
            $token_res = $data['Token'];
            $result = array(
                "Username" => $data['Username'],
                "Token" => $data['Token']
            );
            return $result;
        }else if($status_code=="404"){
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        } 
    }

    public function SuspendUser($username){
        $client = new GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
        
        $dataArray  = array(
            'Method' => "SU",
        'Timestamp' => "$timestamp",
       'Username' => "$username"
        );

        $signature = $this->signature_gen($this->secret,$dataArray);
   

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
      'form_params' => array(
        //   'AppID' => "TF39",
        //     "Signature" => "qpdcrnoddr7m6",
        'Method' => 'SU',
        'Timestamp' => $timestamp,
        'Username' => $username
      ));

      $res=  $client->post(
        $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
    );
        

        //var_dump($res);

        //echo $res->getStatusCode();
    

        $status_code = $res->getStatusCode();


        // if user exists 
        if($status_code=="200"){
            $data = json_decode($res->getBody(),true);
            
           $result = array("success"=>true);

           return $result ;
        }else if($status_code=="404"){
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        }else{
            $result = array(
                "error" => "Error"
            );
            return $result;
        }
    }

    public function SetStatusUser($username,$status){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
        
        $dataArray  = array(
            'Method' => 'SS',
            'Status'=> $status,
            'Timestamp' => $timestamp,
            'Username' => $username
        );

        $signature = $this->signature_gen($this->secret,$dataArray);
   

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
        'form_params' => array(
        //   'AppID' => "TF39",
        //     "Signature" => "qpdcrnoddr7m6",
        'Method' => 'SS',
        'Status'=> $status,
        'Timestamp' => $timestamp,
        'Username' => $username
      ));
      $res=  $client->post(
        $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );


        $status_code = $res->getStatusCode();
        


        // if user exists 
        if($status_code=="200"){
            
           $result = array("success"=>true);
            return $result;
        }else if($status=="404"){
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        }else{
            $result = array(
                "error" => "Error"
            );
            return $result;
        } 
    }

    public function SetPassword($username,$password){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
        
        $dataArray  = array(
            'Method' => 'SP',
            'Password'=> $password,
            'Timestamp' => $timestamp,
            'Username' => $username
        );

        $signature = $this->signature_gen($this->secret,$dataArray);
   

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
        'form_params' => array(
        //   'AppID' => "TF39",
        //     "Signature" => "qpdcrnoddr7m6",
        'Method' => 'SP',
        'Password'=> $password,
        'Timestamp' => $timestamp,
        'Username' => $username
      ));

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );


        $status_code = $res->getStatusCode();


        // if user exists 
        if($status_code=="200"){
            $data = json_decode($res->getBody(),true);
            $result = array(
                "success" => true
            );
            
            return $result;
        }else if($status_code=="404"){
            $result = array(
                "error" => "User does not exist"
            );
            return $result;
        }else{
            $result = array(
                "error" => "Error"
            );
            return $result;
        }
    }

    public function ForwardingToTheGameSite($token,$game){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
        
        $dataArray  = array(
            "token" => $token
        );

        $dataReq = array();
        if(null!=$this->input->post("RequestID")){
            $dataReq = array(
                'game' => $game
            );
        }

        $dataArray = array_merge($dataArray,$dataReq);

        //$signature = $this->signature_gen($this->secret,$dataArray);
   

        $post_data = array(
            // 'AppID' => "TF39",
            // "Signature" => "qpdcrnoddr7m6",
        'form_params' => array(
        //   'AppID' => "TF39",
        //     "Signature" => "qpdcrnoddr7m6",
        "token" => $token,
        'game' => $game
      ));

        $res=  $client->post(
            $this->url_forward, $post_data  //var $url_joker = 'http://api.joker688.net:81/';
        );


        $status_code = $res->getStatusCode();


        // if user exists 
        if($status_code=="200"){
            echo $res->getBody();
            $data = json_decode($res->getBody(),true);
            $result = array(
                "success" => true
            );
            
            return $result;
        
        }else{
            $result = array(
                "error" => "Error"
            );
            return $result;
        }
    }
    
    public  function jokertest(){
        
        echo "HE !!";
    }

    
} 
?>