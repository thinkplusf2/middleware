<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['joker'] = 'Cockfight/index';

$route['joker/user/create_joker'] = 'user/create_joker';



$route['joker/cock/test'] = 'cockfight/test';

$route['joker/user/create'] = 'user/create';
$route['joker/user/setStatus'] = 'user/setStatus';
$route['joker/user/changePassword'] = 'user/changePassword';
$route['joker/user/setUpPassword'] = 'user/setUpPassword';
$route['joker/user/suspend'] = 'user/suspend';
$route['joker/wallet/getBalance'] = 'wallet/getBalance';
$route['joker/wallet/deposit'] = 'wallet/deposit';
$route['joker/wallet/withDraw'] = 'wallet/withdraw';
$route['joker/game/forwardToGame'] = 'game/forwardToGame';



$route['cockfight/user/getKey'] = 'cockfight/GetKey_con';
$route['cockfight/user/login'] = 'cockfight/Login_con';
$route['cockfight/user/mobileLogin'] = 'cockfight/MobileLogin_con';
$route['cockfight/user/logout'] = 'cockfight/Logout_con';
$route['cockfight/user/updateBetSetting'] = 'cockfight/UpdateBetSettings_con';
$route['cockfight/user/getMatchResults'] = 'cockfight/GetMatchResults_con';
$route['cockfight/user/getSchedule'] = 'cockfight/GetSchedule_con';


$route['cockfight/user/create_cockfight'] = 'user/create_cock';
$route['cockfight/user/update'] = 'cockfight/Update_con';
$route['cockfight/user/changePassword'] = 'cockfight/ChangePassword_con';
$route['cockfight/user/suspend'] = 'cockfight/Suspend_con';


$route['cockfight/wallet/getBalance'] = 'cockfight/GetBalance_con';


$route['cockfight/game/getBalance'] = 'cockfight/GetBalance_con2'; //json


$route['cockfight/wallet/deposit'] = 'wallet/deposit';
$route['cockfight/wallet/withDraw'] = 'wallet/withdraw';

$route['cockfight/game/placeBet'] = 'cockfight/PlaceBet_con';
$route['cockfight/game/updateBet'] = 'cockfight/UpdateBet_con';
$route['cockfight/game/cancelBet'] = 'cockfight/CancelBet_con';
$route['cockfight/game/settleMatch'] = 'cockfight/SettleMatch_con';
$route['cockfight/game/voidLateClose'] = 'cockfight/VoidLateClose_con';
$route['cockfight/game/refund'] = 'cockfight/Refund_con';

$route['cockfight/user/delete_cock'] = 'user/delete_cock';



