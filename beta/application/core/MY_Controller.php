<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use \Monolog\Formatter\LineFormatter;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class MY_Controller extends CI_Controller {

    protected $data = Array(); //protected variables goes here its declaration

    // var $url_joker = 'https://www.superbo68.net';
//    var $url_joker = 'http://api.joker688.net:81/';
//    var $secret =  "qpdcrnoddr7m6";
//    var $url_forward = "http://www.joker688.net/";
    //var $url_joker = 'http://www.joker688.net';

    function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE); // I keep this here so I dont have to manualy edit each controller to see profiler or not        
        //load helpers and everything here like form_helper etc
    }

    public function joker_log($funcName, $state, $details, $game) {
        $date = date('d-m-Y');
        $logfile = 'D:/log/joker/Log_Joker_' . $date . '.log';
        $logger = new Logger('Joker_log');
// Now add some handlers
        $logger->pushHandler(new StreamHandler($logfile, Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());

        $msg = "[$funcName] [$game] Details : { $details }";
// You can now use your logger

        switch ($state) {
            case "info":
                $logger->info($msg . "\n");
                break;
            case "debug":
                $logger->debug($msg . "\n");
                break;
            case "alert":
                $logger->alert($msg . "\n");
                break;
            case "critical":
                $logger->critical($msg . "\n");
                break;
            case "emergency":
                $logger->EMERGENCY($msg . "\n");
                break;
            case "error":
                $logger->ERROR($msg . "\n");
                break;
            default:
        }
    }

    public function cockfight_log($funcName, $state, $details, $game) {
        $date = date('d-m-Y');
        $logfile = 'D:/log/cockfight/Log_Cockfight_' . $date . '.log';
        $logger = new Logger('Cockfight_log');
// Now add some handlers
        $logger->pushHandler(new StreamHandler($logfile, Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());

        $msg = "[$funcName] [$game] Details : { $details }";
// You can now use your logger

        switch ($state) {
            case "info":
                $logger->info($msg . "\n");
                break;
            case "debug":
                $logger->debug($msg . "\n");
                break;
            case "alert":
                $logger->alert($msg . "\n");
                break;
            case "critical":
                $logger->critical($msg . "\n");
                break;
            case "emergency":
                $logger->EMERGENCY($msg . "\n");
                break;
            case "error":
                $logger->ERROR($msg . "\n");
                break;
            default:
        }
    }
    
     public function signature_gen($secretF, $key) {
        //$this->secret = "qpdcrnoddr7m6"
        // $key must be array ;
        $text = "";
        ksort($key);
        while ($key_name = current($key)) {
            $text .= key($key) . '=' . $key_name . '&';
            next($key);
        }
        // again 

        $text = substr($text, 0, -1);

        $signature = base64_encode(hash_hmac("SHA1", $text, $secretF, true));

        //$signature = base64_encode(hash_hmac("SHA1",$key, $this->secret));
        //return substr( $signature, 0, -2);
        return $signature;
    }

}
