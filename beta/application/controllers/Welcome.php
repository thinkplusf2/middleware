<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
require 'vendor/autoload.php';
class Welcome extends CI_Controller {


    public function index() {
        $this->load->view('welcome_message');
    }

}
