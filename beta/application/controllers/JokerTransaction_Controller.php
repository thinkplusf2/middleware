<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use \Monolog\Formatter\LineFormatter;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

require 'vendor/autoload.php';

class JokerTransaction_Controller extends MY_Controller {

    var $secret = 'qpdcrnoddr7m6';
    var $url_joker = 'http://api.joker688.net:81';

    public function __construct() {
        parent::__construct();
        $this->load->library('customautoloader');
        $this->load->model("joker_model");
    }

    public function index() {

        echo "Hello";
    }

    public function testtime() {
        date_default_timezone_set("Asia/Bangkok");

        $temp_time = date("Y-m-d H:i");

        $StartDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60);
        $EndDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60 + 10 * 60);
        echo $StartDate . " / " . $EndDate;
    }

    public function RetrieveTransaction() {
        header('Content-Type: application/json');
        date_default_timezone_set("Asia/Bangkok");

        $timeZone = "SA%20Western%20Standard%20Time";
        $StartDate = $this->input->post('StartDate');
        $EndDate = $this->input->post('EndDate');



        if ($StartDate == null || $EndDate == null) {
            $temp_time = date("Y-m-d H:i");
            //$StartDate = date('Y-m-d H:i', $temp_time);
            $StartDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60 + 10 * 60);
        } else {
            $StartDate = date('Y-m-d H:i', strtotime($StartDate) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($EndDate) + 60 * 60);
        }



        do {
            // request to api server 
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            $dataArray = array(
                'EndDate' => $EndDate,
                'Method' => "TS",
                'StartDate' => $StartDate,
                'Timestamp' => $timestamp
            );

            $this->joker_log("RetrieveTransaction", "info", json_encode($dataArray), "Joker");


            $post_data = array(
                'form_params' => $dataArray
            );

            $signature = $this->signature_gen($this->secret, $dataArray);

            $res = $client->post(
                    $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data
            );

            $status_code = $res->getStatusCode();
        } while ($status_code != 200);

        $data = json_decode($res->getBody(), true);

        if (empty($data["data"])) {
            // Log::info('transactJokerMessage is null data', array('message' => $data));
        } else {
            $result = $data["data"]["Game"];
            $re = $this->joker_model->GetJokerTransaction($result, $timeZone);
        }

        $this->joker_log("RetrieveTransaction", "info", json_encode($data), "Joker");

        echo json_encode($data);
    }

    public function TransactionTotal() {
        header('Content-Type: application/json');

        ini_set('max_execution_time', 300);
        date_default_timezone_set("Asia/Bangkok");
        $EndDate = $this->input->post("EndDate");
        $StartDate = $this->input->post("StartDate");
        if ($StartDate == null || $EndDate == null) {

            $temp_time = date("Y-m-d H:i");
            $StartDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60 + 10 * 60);
            //echo $StartDate . " " . $EndDate;
        } else {
            $StartDate = date('Y-m-d H:i', strtotime($StartDate) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($EndDate) + 60 * 60);
        }
        $userss = $this->input->post("user");


        $value = $this->sendRequestAndStoreDB($StartDate, $EndDate, $userss);

        $this->joker_log("TransactionTotal", "info", json_encode($value), "Joker");

        echo json_encode($value);
    }

    public function sendRequestAndStoreDB($StartDate, $EndDate, $user) {

        date_default_timezone_set("Asia/Bangkok");

        do {
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            $dataArray = array(
                'EndDate' => $EndDate,
                'Method' => "TRX",
                'StartDate' => $StartDate,
                'Timestamp' => $timestamp,
                'Username' => $user
            );

            $post_data = array(/* $dataArray */
                'form_params' => $dataArray
            );
            $arr = array(
                "input : " => $dataArray
            );
            $this->joker_log("TransactionTotal", "info", json_encode($arr), "Joker");

            $signature = $this->signature_gen($this->secret, $dataArray);

            $res = $client->post(
                    $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data
            );

            $status_code = $res->getStatusCode();
        } while ($status_code != 200);

        $data = json_decode($res->getBody(), true);
        // var_dump($data);
        $result = $data["data"];
        if (empty($result)) {
            
        } else {
            $re = $this->joker_model->GetJokerTotalTransaction($result);
        }

        return ($data);
    }

    public function GetJackpot() {
        header('Content-Type: application/json');

        date_default_timezone_set("Asia/Bangkok");
        do {
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            $dataArray = array(
                'Method' => "JP",
                'Timestamp' => $timestamp
            );

            $post_data = array(
                'form_params' => $dataArray
            );
            $arr = array(
                "input : " => $dataArray
            );
            $this->joker_log("GetJackpot", "info", json_encode($arr), "Joker");


            $signature = $this->signature_gen($this->secret, $dataArray);

            $res = $client->post(
                    $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data
            );

            $status_code = $res->getStatusCode();
        } while ($status_code != 200);

        $data = json_decode($res->getBody(), true);
        $result = $data;
        if (empty($result)) {
            
        } else { /* insert transaction JokerGame DB */
            $this->joker_model->GetJokerJackpot($result);
        }
        $this->joker_log("GetJackpot", "info", json_encode($data), "Joker");
        echo json_encode($data);
    }

    public function GetJackpotGames() {
        header('Content-Type: application/json');
        date_default_timezone_set("Asia/Bangkok");

        do {
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            $dataArray = array(
                'Method' => "GJP",
                'Timestamp' => $timestamp
            );

            $post_data = array(
                'form_params' => $dataArray
            );

            $signature = $this->signature_gen($this->secret, $dataArray);
            $arr = array(
                "input : " => $dataArray
            );
            $this->joker_log("GetJackpotGames", "info", json_encode($arr), "Joker");

            $res = $client->post(
                    $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data
            );

            $status_code = $res->getStatusCode();
        } while ($status_code != 200);

        $data = json_decode($res->getBody(), true);

        if (empty($data["Jackpots"])) {
            // Log::info('transactJokerMessage is null data', array('message' => $data));
        } else {
            
            $result = $data["Jackpots"];
            $this->joker_model->GetJokerJackpotGame($result);
            
        }
        $array_result = array(
            "result" => "successed"
        );
        $this->joker_log("GetJackpotGames", "info", json_encode($data), "Joker");

        echo json_encode($data);
    }

    public function transactionSummary() {
        header('Content-Type: application/json');
//        date_default_timezone_set("Asia/Bangkok");
//        
        $StartDate = $this->input->post('StartDate');
        $EndDate = $this->input->post('EndDate');

        $userss = $this->input->post('username');


        $array = array(
            "username" => $userss,
            "StartDate" => $StartDate,
            "EndDate" => $EndDate
        );
        $res = array();
        $res = $this->joker_model->transactionSummary($array);

        echo json_encode($res);
    }

}
