<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use \Monolog\Formatter\LineFormatter;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

require 'vendor/autoload.php';

class CockfightTransaction_Controller extends MY_Controller {

    var $secret = 'qpdcrnoddr7m6';
    // var $url_joker = 'http://api.joker688.net:81'; url_cock
    // var $url_cock = 'http://203.69.34.177:7749/api/hiwinbet/';
    // var $url_cock = 'http://test.sv33888.com:7748/api/hiwinbet/';
    var $url_cock = 'http://www.sv33888.com/api/hiwinbet/';
    var $cert = "hi5BDE7RJgIzYzZ9";

    public function __construct() {
        parent::__construct();
        $this->load->library('customautoloader');
        $this->load->model("cockfight_model");
    }

    public function index() {

        echo "Hello";
    }

    public function GetTransacEventDate() {
        header('Content-Type: application/json');

        $cert = $this->input->post("cert");
        if ($cert == null) {
            $cert = $this->cert;
        }
        $user = $this->input->post("user");
        $startdate = $this->input->post("startdate");
        $enddate = $this->input->post("enddate");


        if ($startdate == null || $enddate == null) {

            $startdate = date("d/m/Y");
            $enddate = date("d/m/Y");
        }

        $dataArray = array(
            'cert' => $cert,
            'user' => $user,
            'startdate' => $startdate,
            'enddate' => $enddate,
            'extension1' => $this->input->post('extension1'),
            'extension2' => $this->input->post('extension2'),
            'extension3' => $this->input->post('extension3')
        );

        $this->cockfight_log("GetTransacEventDate", "info", json_encode($dataArray), "Cockfight");

        $url_GetTranUpdate = $this->url_cock . "getTransactionByEventDate";
        $client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $post_data = array(
            'form_params' => $dataArray
        );

        $res = $client->post(
                $url_GetTranUpdate, $post_data
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(), true);

        if (isset($data['transactions'])) {
            $result = $data["transactions"];

            $this->cockfight_model->GetTransactionByEventDate($result);
        } // end of isset

        $this->cockfight_log("GetTransacEventDate", "info", json_encode($data), "Cockfight");

        echo json_encode($data);
    }

    public function GetTransacLastUpdateDate() {

        header('Content-Type: application/json');

        $cert = $this->input->post('cert');
        if ($cert == null) {
            $cert = $this->cert;
        }
        $user = $this->input->post('user');
        $lastupdatedate = $this->input->post('lastupdatedate');
        if($lastupdatedate==null){
            $lastupdatedate = "0";
        }else{
            $lastupdatedate = strtotime($lastupdatedate);
        }
        // $cert = "hi5BDE7RJgIzYzZ9";
        // $user = "hthb0101";
        // $lastupdatedate = "0";

        $dataArray = array(
            'cert' => $cert,
            'user' => $user,
            'lastupdatedate' => $lastupdatedate
        );

        // recieve optional variable
        $nanos = $this->input->post('nanos');


        $url_GetTranUpdate = $this->url_cock . "getTransactionByLastUpdateDate";
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        $dataArray = array(
            'cert' => $cert,
            'user' => $user,
            'lastupdatedate' => $lastupdatedate,
            'nanos' => $this->input->post('nanos'),
            'extension1' => $this->input->post('extension1'),
            'extension2' => $this->input->post('extension2'),
            'extension3' => $this->input->post('extension3')
        );

        $arr = array(
            "input : " => $dataArray
        );

        $this->cockfight_log("GetTransacLastUpdateDate", "info", json_encode($dataArray), "Cockfight");


        $post_data = array(
            'form_params' => $dataArray
        );

        $res = $client->post(
                $url_GetTranUpdate, $post_data
        );



        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(), true);
        $result = $data["transactions"];

        if (isset($data['transactions'])) {

            //$this->cockfight_model->GetTransactionLastUpdate($result);
        }

        //echo $url_GetTranUpdate . "<br>";
        //var_dump($post_data);
        $this->cockfight_log("GetTransacLastUpdateDate", "info", json_encode($data), "Cockfight");

        echo json_encode($data);
        // return($data);
    }

    public function CockfightGetTransaction() {
        header('Content-Type: application/json');

        $username = $this->input->post("username");
        $StartDate = $this->input->post("StartDate");
        $EndDate = $this->input->post("EndDate");

        $array = array(
            "username" => $username,
            "StartDate" => $StartDate,
            "EndDate" => $EndDate
        );

        $res = $this->cockfight_model->GetCockfightTransaction($array);

        echo json_encode($res);
    }
    
    public function cockfight_log($funcName, $state, $details, $game) {
        $date = date('d-m-Y');
        $logfile = 'D:/log/cockfight/Log_Cockfight_' . $date . '.log';
        $logger = new Logger('Cockfight_log');
        
        $logger->pushHandler(new StreamHandler($logfile, Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());


        $msg = "[$funcName] [$game] Details : { $details }";
// You can now use your logger

        switch ($state) {
            case "info":
                $logger->info($msg . "\n");
                break;
            case "debug":
                $logger->debug($msg . "\n");
                break;
            case "alert":
                $logger->alert($msg . "\n");
                break;
            case "critical":
                $logger->critical($msg . "\n");
                break;
            case "emergency":
                $logger->EMERGENCY($msg . "\n");
                break;
            case "error":
                $logger->ERROR($msg . "\n");
                break;
            default:
        }
        
        
    }

}
