<?php

//use Wrapper\JokerApiWrapper;


class joker_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->model("joker_model");
    }

    public function GetJokerTransaction($result, $timeZone) {

        for ($i = 0; $i < count($result); $i++) {
            // check record in DB
            $db_c_time = strtotime(str_replace('T', ' ', $result[$i]["Time"]));

            // $temp_time = strtotime(str_replace('T', ' ', $result[$i]["Time"])); 
            $db_r_time = date("Y-m-d H:i", $db_c_time);

            //$checkRecordTransac = $this->db->select("*")->from("transactionjoker")->where(array("OCode" => $result[$i]["OCode"]))->get();
            $checkRecordTransac = $this->db->select("*")->from("transactionjoker")
                    ->where(array("OCode" => $result[$i]["OCode"]))
                    ->get();

            if ($checkRecordTransac->num_rows() > 0) { // update transaction JokerGame DB        !is_null($checkRecordTransac)    
//                        echo $result[$i]["Username"];
//                        echo ">>>> 11111111<br>";
                foreach ($checkRecordTransac->result() as $row) {
                    $r_time = strtotime(str_replace('T', ' ', $result[$i]["Time"]));

                    // $temp_time = strtotime(str_replace('T', ' ', $result[$i]["Time"])); 
                    $c_time = date("Y-m-d H:i", $r_time);
                    if ($timeZone == null) {
                        $this->db->set(array(
                            'OCode' => $result[$i]["OCode"],
                            'Username' => $result[$i]["Username"],
                            'GameCode' => $result[$i]["GameCode"],
                            'Description' => $result[$i]["Description"],
                            'Type' => $result[$i]["Type"],
                            'Amount' => $result[$i]["Amount"],
                            'Result' => $result[$i]["Result"],
                            'Time' => $c_time,
                            'AppID' => $result[$i]["AppID"],
                            'CurrencyCode' => $result[$i]["CurrencyCode"],
                            // 'EndDate' => $dataAraay['EndDate'],
                            'Details' => $result[$i]["Details"]
                        ));
                    } else {
                        $this->db->set(array(
                            'OCode' => $result[$i]["OCode"],
                            'Username' => $result[$i]["Username"],
                            'GameCode' => $result[$i]["GameCode"],
                            'Description' => $result[$i]["Description"],
                            'Type' => $result[$i]["Type"],
                            'Amount' => $result[$i]["Amount"],
                            'Result' => $result[$i]["Result"],
                            'Time' => $c_time,
                            'AppID' => $result[$i]["AppID"],
                            'CurrencyCode' => $result[$i]["CurrencyCode"],
                            'timeZone' => $timeZone,
                            // 'EndDate' => $dataAraay['EndDate'],
                            'Details' => $result[$i]["Details"]
                        ));
                    }

                    $this->db->where("idTranGame", $row->idTranGame);
                    $updateDB = $this->db->update("transactionjoker");
                }

                //Log::info('update transaction JokerGame ', array('update' => $result));
            } else {
// insert transaction JokerGame DB            

                $r_time = strtotime(str_replace('T', ' ', $result[$i]["Time"]));

                // $temp_time = strtotime(str_replace('T', ' ', $result[$i]["Time"])); 
                $c_time = date("Y-m-d H:i", $r_time);
                $data = array(
                    'OCode' => $result[$i]["OCode"],
                    'Username' => $result[$i]["Username"],
                    'GameCode' => $result[$i]["GameCode"],
                    'Description' => $result[$i]["Description"],
                    'Type' => $result[$i]["Type"],
                    'Amount' => $result[$i]["Amount"],
                    'Result' => $result[$i]["Result"],
                    'Time' => $c_time,
                    'AppID' => $result[$i]["AppID"],
                    'CurrencyCode' => $result[$i]["CurrencyCode"],
                    'timeZone' => $timeZone,
                    //'EndDate' => $EndDate,
                    'Details' => $result[$i]["Details"]
                );
                $insertDB = $this->db->insert("transactionjoker", $data);
                // Log::info('insert transaction JokerGame', array('insert' => $result));
            }

            // var_dump($checkRecordTransac); 
        }



//        $return = array(
//            "status" => $return
//        );

        return 1;
    }

    public function GetJokerTotalTransaction($result) {
        for ($i = 0; $i < count($result); $i++) {
            // check record in DB
            $r_time = strtotime(str_replace('T', ' ', $result[$i]["Date"]));
            $c_time = date("Y-m-d H:i", $r_time);
            $checkRecordTransac = $this->db->select("*")->from("transactotaljoker")->where(array(
                        "PersonOCode" => $result[$i]["PersonOCode"],
                        "Username" => $result[$i]["Username"],
                        "Date" => $c_time
                    ))->get();


            if ($checkRecordTransac->num_rows() > 0) { // update transaction JokerGame DB  
                foreach ($checkRecordTransac->result() as $row1) {
                    $r_time = strtotime(str_replace('T', ' ', $result[$i]["Date"]));
                    $c_time = date("Y-m-d H:i", $r_time);

                    $this->db->set(array(
                        'Date' => $c_time,
                        'PersonOCode' => $result[$i]["PersonOCode"],
                        'Username' => $result[$i]["Username"],
                        'CurrenyCode' => $result[$i]["CurrenyCode"],
                        'TotalAmount' => $result[$i]["TotalAmount"],
                        'TotalResult' => $result[$i]["TotalResult"],
                        'TotalDeposit' => $result[$i]["TotalDeposit"],
                        'TotalWithdraw' => $result[$i]["TotalWithdraw"]
                    ));
                    $this->db->where(array("Date" => $row->Date));
                    $updateDB = $this->db->update("transactotaljoker");
                }
            } else { // insert transaction JokerGame DB     
                $r_time = strtotime(str_replace('T', ' ', $result[$i]["Date"]));
                $c_time = date("Y-m-d H:i", $r_time);

                $data2 = array(
                    'Date' => $c_time,
                    'PersonOCode' => $result[$i]["PersonOCode"],
                    'Username' => $result[$i]["Username"],
                    'CurrenyCode' => $result[$i]["CurrenyCode"],
                    'TotalAmount' => $result[$i]["TotalAmount"],
                    'TotalResult' => $result[$i]["TotalResult"],
                    'TotalDeposit' => $result[$i]["TotalDeposit"],
                    'TotalWithdraw' => $result[$i]["TotalWithdraw"]
                );
                $insertDB = $this->db->insert("transactotaljoker", $data2);
            }
        }

        return 1;
    }

    public function GetJokerJackpot($result) {

        $insert = $this->db->insert("jackpot", array("Amount" => $result['Amount']));

        return 1;
    }

    public function GetJokerJackpotGame($result) {

        for ($i = 0; $i < count($result); $i++) {
            // check record in DB

            $checkRecordTransac = $this->db->select("*")->from("jackpotgamesjoker")->where("GameCode", $result[$i]["GameCode"])->get();


            if ($checkRecordTransac->num_rows() > 0) { // update Jackpot JokerGame DB        
                foreach ($checkRecordTransac->result() as $row) {

                    $this->db->set(array(
                        'GameCode' => $result[$i]["GameCode"],
                        'Amount' => $result[$i]["Amount"]
                    ));
                    $this->db->where("GameCode", $row->GameCode);
                    $updateDB = $this->db->update("jackpotgamesjoker");
                }

            } else { // insert Jackpot JokerGame DB            
                $ar = array(
                    'GameCode' => $result[$i]["GameCode"],
                    'Amount' => $result[$i]["Amount"]
                );
                $insertDB = $this->db->insert("jackpotgamesjoker", $ar);

            }

            return 1;
            // var_dump($result[$i]);
        }
    }

    public function transactionSummary($array) {
        $StartDate = date('Y-m-d H:i:s', strtotime($array['StartDate']));
        $EndDate = date('Y-m-d H:i:s', strtotime($array['EndDate']) + 60 * 60 * 24);
        if ($array['username'] != null) {

            $username = $array['username'];
            $qu = "SELECT * FROM transactionjoker where timestamp >= " . $this->db->escape($StartDate) . " and timestamp < " . $this->db->escape($EndDate) . " && Username = " . $this->db->escape($username) . " ;";
            //$transacSum = $this->db->select("*")->from("transactionjoker")->where(array("userId"=>$array['username']))->get();
            $transacSum = $this->db->query($qu);
        } else {
            $qu = "SELECT * FROM transactionjoker where timestamp > " . $this->db->escape($StartDate) . " and timestamp < " . $this->db->escape($EndDate) . ";";
            //$transacSum = $this->db->select("*")->from("transactionjoker")->where(array("userId"=>$array['username']))->get();
            $transacSum = $this->db->query($qu);
        }

        if ($transacSum->num_rows() > 0) {


            $res2 = array();
            $res = [];

            // var_dump($transacSum->result());
            $res = $transacSum->result();
        } else {
            $res = array(
                "message" => "no data"
            );
        }

//        $res = array(
//          "Start" => $StartDate,
//            "End" => $EndDate
//        );

        return $res;
    }

}
