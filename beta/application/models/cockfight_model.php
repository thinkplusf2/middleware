<?php

use GuzzleHttp\Client;

//use Wrapper\JokerApiWrapper;
require 'vendor/autoload.php';

class cockfight_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        require 'vendor/autoload.php';
        $this->load->model("cockfight_model");
    }

    public function GetTransactionByEventDate($result) {

        for ($i = 0; $i < count($result); $i++) {
            // check record in DB
            $checkRecordTransac = $this->db->select("*")->from("transaction_cf_eventdate")->where(array(
                        "userid" => $result[$i]["userid"],
                        "transactionno" => $result[$i]["transactionno"]
                    ))->get();
            $tr = strtotime($result[$i]['transactiontime']);
            $tran_time = date('Y-m-d H:i:s', $tr);
            $d = strtotime($result[$i]["eventdate"]);
            $eventdate = date('Y-m-d H:i:s', $d);
            $d2 = strtotime($result[$i]["updatedate"]);
            $updatedate = date('Y-m-d H:i:s', $d2);

            if ($checkRecordTransac->num_rows() > 0) { // update transaction JokerGame DB
                foreach ($checkRecordTransac->result() as $row) {
                    $dataA = array(
                        'userid' => $result[$i]["userid"],
                        'transactionno' => $result[$i]["transactionno"],
                        'transactiontime' => $tran_time,
                        'eventdate' => $eventdate,
                        'updatedate' => $updatedate,
                        'arena' => $result[$i]["arena"],
                        'matchno' => $result[$i]["matchno"],
                        'choice' => $result[$i]["choice"],
                        'stake' => $result[$i]["stake"],
                        'status' => $result[$i]["status"],
                        'result' => $result[$i]["result"],
                        'settled' => $result[$i]["settled"],
                        'winloss' => $result[$i]["winloss"],
                        'commission' => $result[$i]["commission"]
                    );
                    $this->db->set($dataA);
                    $this->db->where("transactionno", $row->transactionno);
                    $updateDB = $this->db->update("transaction_cf_eventdate");
                }

                //  Log::info('update transaction cockfight ', array('update' => $result));
            } else { // insert transaction JokerGame DB
                $dataA = array(
                    'userid' => $result[$i]["userid"],
                    'transactionno' => $result[$i]["transactionno"],
                    'transactiontime' => $tran_time,
                    'eventdate' => $eventdate,
                    'updatedate' => $updatedate,
                    'arena' => $result[$i]["arena"],
                    'matchno' => $result[$i]["matchno"],
                    'choice' => $result[$i]["choice"],
                    'odds' => $result[$i]["odds"],
                    'stake' => $result[$i]["stake"],
                    'status' => $result[$i]["status"],
                    'result' => $result[$i]["result"],
                    'settled' => $result[$i]["settled"],
                    'winloss' => $result[$i]["winloss"],
                    'commission' => $result[$i]["commission"]
                );

                $insertDB = $this->db->insert("transaction_cf_eventdate", $dataA);

                //Log::info('insert transaction cockfight', array('insert' => $result));
            }
        }
        return 1;
    }

    public function GetTransactionLastUpdate($result) {
        for ($i = 0; $i < count($result); $i++) {
            // check record in DB
            $checkRecordTransac = $this->db->select("*")->from("transaction_cf_lastupdate")->where(array(
                        "userid" => $result[$i]["userid"],
                        "transactiontime" => $result[$i]["transactiontime"]
                    ))->get();

            $tr = strtotime($result[$i]['transactiontime']);
            $tran_time = date('Y-m-d H:i:s', $tr);
            $d = strtotime($result[$i]["eventdate"]);
            $eventdate = date('Y-m-d H:i:s', $d);
            $d2 = strtotime($result[$i]["updatedate"]);
            $updatedate = date('Y-m-d H:i:s', $d2);


            if ($checkRecordTransac->num_rows() > 0) { // update transaction          
                foreach ($checkRecordTransac->result() as $row) {
                    $dataA = array(
                        'userid' => $result[$i]["userid"],
                        'transactionno' => $result[$i]["transactionno"],
                        'transactiontime' => $tran_time,
                        'matchno' => $result[$i]["matchno"],
                        'choice' => $result[$i]["choice"],
                        'stake' => $result[$i]["stake"],
                        'status' => $result[$i]["status"],
                        'result' => $result[$i]["result"],
                        'winloss' => $result[$i]["winloss"],
                        'commission' => $result[$i]["commission"],
                        'lastupdatedate' => $result[$i]["lastupdatedate"],
                        'nanos' => $result[$i]["nanos"],
                        'settled' => $result[$i]["settled"],
                        'eventdate' => $eventdate,
                        'updatedate' => $updatedate,
                        'arena' => $result[$i]["arena"],
                        'odds' => $result[$i]["odds"]
                    );

                    $this->db->set($dataA);
                    $this->db->where(array(
                        "transactionno" => $row->transactionno
                    ));
                    $updateDB = $this->db->update("transaction_cf_lastupdate");
                }

                //Log::info('update transaction by last update cockfight ', array('update' => $result));
            } else { // insert transaction     
                $dataA = array(
                    'userid' => $result[$i]["userid"],
                    'transactionno' => $result[$i]["transactionno"],
                    'transactiontime' => $tran_time,
                    'matchno' => $result[$i]["matchno"],
                    'choice' => $result[$i]["choice"],
                    'stake' => $result[$i]["stake"],
                    'status' => $result[$i]["status"],
                    'result' => $result[$i]["result"],
                    'winloss' => $result[$i]["winloss"],
                    'commission' => $result[$i]["commission"],
                    'lastupdatedate' => $result[$i]["lastupdatedate"],
                    'nanos' => $result[$i]["nanos"],
                    'settled' => $result[$i]["settled"],
                    'eventdate' => $eventdate,
                    'updatedate' => $updatedate,
                    'arena' => $result[$i]["arena"],
                    'odds' => $result[$i]["odds"]
                );
                $insertDB = $this->db->insert("transaction_cf_lastupdate", $dataA);

                //Log::info('insert transaction JokerGame', array('insert' => $result));
            }
        }

        return 1;
    }

    public function GetCockfightTransaction($array) {

        $StartDate = date('Y-m-d H:i:s', strtotime($array['StartDate']));
        $EndDate = date('Y-m-d H:i:s', strtotime($array['EndDate']) + 60 * 60 * 24);


//        $where = array(
//            "userid" => $array['username'],
//            "transactiontime >=" => $StartDate,
//            "transactiontime <" => $EndDate
//        );
//        $where = "userid = ". $this->db->escape($array['username']).""
//                . " AND transactiontime >= ". $this->db->escape($StartDate).""
//                . " AND transactiontime <". $this->db->escape($EndDate)."";

        if ($array['username' != null]) {
            $select = "SELECT * FROM transaction_cf_eventdate where userid = " . $this->db->escape($array['username']) . " "
                    . "AND transactiontime >=" . $this->db->escape($StartDate) . " AND transactiontime <" . $this->db->escape($EndDate) . ";";
        } else {
            $select = "SELECT * FROM transaction_cf_eventdate where transactiontime >=" . $this->db->escape($StartDate) . ""
                    . " AND transactiontime <" . $this->db->escape($EndDate) . ";";
        }
        $qu = $this->db->query($select);

        //echo $select;
        //$qu = $this->db->select("*")->from("transaction_cf_eventdate")->where($where)->get();
        if ($qu->num_rows() > 0) {
            return $qu->result();
        } else {
            return array("error" => "no data");
        }



//        return array(
//            "StartDate" => $StartDate,
//            "EndDate" => $EndDate
//        );
    }

}
