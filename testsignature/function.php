<?php

if(isset($_POST['func'])){
    if($_POST['func']=="token"){
        gen_uuid();
    }else if($_POST['func']=="sig"){
        signature_gen();
    }
}



function signature_gen() {
    header('Access-Control-Allow-Origin: *'); 

    header('Content-Type: application/json');

//$secret = "qpdcrnoddr7m6"
// $key must be array ;
    $passkey = $_POST['passkey'];
    $key = array(
        "currency" => $_POST['currency'],
        "externalToken" => $_POST['externalToken'],
        "gameType" => $_POST['gameType'],
        "lang" => $_POST['lang'],
        "nickname" => $_POST['nickname'],
        "playerID" => $_POST['playerID'],
        "siteID" => $_POST['siteID'],
        "skinID" => $_POST['skinID']
    );
    $text = "";
    ksort($key);
    while ($key_name = current($key)) { 

        $text .=$key_name;

        next($key);
    }

    $text .=  $_POST['passkey']; 


    $signature = hash_hmac("SHA512", $text, true);


    echo json_encode(array("before"=> $text,"after"=> $signature)) ;
}

function gen_uuid() {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *'); 
    $str = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
        mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );

    $token =  clean($str);
        //echo $token;
    echo json_encode(array("token"=>$token));
}

function clean($string) {
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        //return preg_replace('~&#x([0-9A-F]+);~i', '', $string); // Removes special chars.
        return strtoupper($string);
        //return $string;
    }

    ?>
