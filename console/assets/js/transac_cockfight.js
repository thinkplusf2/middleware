
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

$(document).ready(function () {
    $("#btn_get").click(function () {
        var date = $("#date").val();
       // console.log(date);
        //var end = $("#endDate").val();
        var formData = new FormData($('#tran_form')[0]);
        $.ajax({
            //url: baseUrl + "/app/console/get_user_data",
            //url: baseUrl + "/feeder/public/transactionSummary",
            url: baseUrl + "/beta/CockfightTransaction_Controller/CockfightGetTransaction",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                $("#result").html("");
                console.log(data);
                if (data.error === null) {
                    alert("no data");

                } else {

                    var str = "";
                    $.each(data, function (idx, obj) {

                        var userid = obj.userid;
                        var tran_time = obj.transactiontime;
                        var tran_no = obj.transactionno;
                        var eventdate = obj.eventdate;
                        var updatedate = obj.updatedate;
                        var arena = obj.arena;
                        var matchno = obj.matchno;
                        var choice = obj.choice;
                        var odds = obj.odds;
                        var stake = obj.stake;
                        var status = obj.status;
                        var result = obj.result;
                        var settled = obj.settled;
                        var winloss = obj.winloss;
                        var commission = obj.commission;
                        var timestamp = obj.timestamp;

                        str += "userid : " + userid + "<br>"
                        str += "transactiontime : " + tran_time + " <br>";
                        str += "transactionno : " + tran_no + " <br>";
                        str += "eventdate : " + eventdate + " <br>";
                        str += "updatedate : " + updatedate + " <br>";
                        str += "arena : " + arena + "<br>";
                        str += "matchno : " + matchno + "<br>";
                        str += "choice : " + choice + "<br>";
                        str += "odds : " + odds + "<br>";
                        str += "stake : " + stake + "<br>";
                        str += "status : " + status + "<br>";
                        str += "result : " + result + "<br>";
                        str += "settled : " + settled + "<br>";
                        str += "winloss : "+winloss+"<br>";
                        str += "commission : "+commission+"<br>";
                        str += "timestamp : "+timestamp+"<br>";
                        str += "<hr>";

                        console.log(obj.username);
//                    console.log(Time);
//                    console.log(detail);
                    });
                    $("#result").css("display", "block");
                    $("#result").html(str);
                }
            }
        });

    });
});