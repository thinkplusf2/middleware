
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Transaction Cockfight</title>
        <link href="<?php echo base_url('assets/css/mycss.css') ?>" rel="stylesheet">

        <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
        crossorigin="anonymous"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">




        <script src="<?php echo base_url('assets/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js') ?>" ></script>
        <link href="<?php echo base_url('assets/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css') ?>" rel="stylesheet">







    </head>
    <body>

        <script  language="JavaScript" type="text/javascript" src="<?php echo base_url('assets/js/transac_cockfight.js') ?>"></script>

        <div class="container" >
            <h1 style="text-align: center;color: #ff3333">Transaction Cockfight</h1>
            
            <div class="col-md-2"></div>
            <div style="text-align: center;margin-top: 30px;" class="col-md-8">
                <form id="tran_form" class="form-horizontal" >
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3  col-md-offset-2">Username :</label>
                            <div class="col-md-4">
                                <input type="text" id="username" class="form-control" name="username" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-md-offset-2">StartDate :</label>
                            <div class="col-md-4 ">
                                <input size="16" type="date" id="StartDate" class="form-control" name="StartDate"  >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-md-offset-2">EndDate :</label>
                            <div class="col-md-4 ">
                                <input size="16" type="date" id="EndDate" class="form-control" name="EndDate"  >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <input type="hidden" id="timeZone" name="timeZone" value="SA%20Western%20Standard%20Time">
                        
                    </div>



                </form>

                <div style="padding:20px;text-align: center">
                    <button id="btn_get" style="padding:5px 15px 5px 15px;" > Submit </button>
                </div>
            </div>
            <br>
            <div style="display: none;margin: 30px;" class="col-md-10" id="result">

            </div>
        </div>
        <script type="text/javascript">
            var format = {
                format: "yyyy-mm-dd hh:ii",
                autoclose: true,
                todayBtn: true,
                todayHighlight: true,
            };
            //$("#date").datetimepicker(format);

            //$("#endDate").datetimepicker(format);
        </script>  
    </body>
</html>