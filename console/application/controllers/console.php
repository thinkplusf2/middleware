<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class console extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        echo "hello No";
    }

    public function console_page() {
        $this->load->view("page/console_page");
    }

    public function forward_joker() {
        $username = $this->input->post("username_login");

        $data['user'] = $username;

        $this->load->view("page/forward_joker", $data);
    }

    public function joker_transaction() {

        $this->load->view("page/tran_joker");
    }

    public function test_log() {
        $datenow = date("dmy-hhmmii") . "";
        //$myfile = fopen("C:/log/logs.txt", "wr") or die("Unable to open file!");
        $txt = "TOPTENa";
        $file = 'C:/log/logs.txt';
        file_put_contents($file, "\n" . $txt, FILE_APPEND);
//        $current = file_get_contents($file);
//        var_dump($current) ;
//        $file = 'C:/log/logs.txt';
//// Open the file to get existing content
//        $current = file_get_contents($file);
//// Append a new person to the file
//        $current .= "John Smith\n";
//// Write the contents back to the file
//        file_put_contents($file, $current);
    }

    public function test_time() {
        date_default_timezone_set("Asia/Bangkok");
        //date_default_timezone_set("Asia/Bangkok");
//        $StartDate = date("Y-m-d H:i");
//        $EndDate = date('Y-m-d H:i', strtotime('+1 hour', $StartDate));
       $temp_time = date("Y-m-d H:is");
        //$StartDate = date('Y-m-d H:i', $temp_time);
        $StartDate = date('Y-m-d H:is', strtotime($temp_time) - 10 * 60);
        $EndDate = $temp_time;
        echo $StartDate . " " . $EndDate;
    }
    
    public function cockfight_transaction(){
        
        $this->load->view("page/cockfight_tran");
    }
    
    public function pornhub_transaction(){
        $this->load->view("page/pornhub_tran");
    }
    
    
}
