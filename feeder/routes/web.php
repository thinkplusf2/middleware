<?php
 
use Illuminate\Http\Request;

// use App\Jobs\logfile_delay;
// use App\User;
use App\Http\Controllers\Controller; 

Route::any('/RetrieveTransactions', 'CreateServiceTransac@RetrieveTransaction');

Route::any('/TransactionTotal', 'CreateServiceTransac@TransactionTotal');

Route::any('/GetJackpot', 'CreateServiceTransac@GetJackpot');

Route::any('/GetJackpotGames', 'CreateServiceTransac@GetJackpotGames');

Route::any('/transactionSummary','CreateServiceTransac@transactionSummary');
Route::any('/testdb_null','CreateServiceTransac@testdb_null');

Route::any('/test_null','CreateServiceTransac@test_null');


Route::any('/GetTransacLastUpdateDate','ServiceTransacCock@GetTransacLastUpdateDate');

Route::any('/GetTransacEventDate','ServiceTransacCock@GetTransacEventDate');


// Route::any('/GetTransacEventDate2', function () {

//     $data = ServiceTransacCock::GetTransacLastUpdateDate();
// });

Route::get('/', function () {
	//echo "string";
     return view('welcome');
});

Route::get('/test', function () {
	//echo "string";
     return view('test');
});
/* call to kernel 

Route::post('/user', function(Request $request)
{
	$callFunc = $request->input('callFunc'); 
	$strComm = 'email:user { Callfunction :'. $callFunc.'}';
	$strComm = 'email:user';
	// Artisan::command($strComm);

    // $exitCode = Artisan::call($strComm);
    Artisan::call($strComm);
});

*/
Route::any('/test_funcGet', 'test_Controller@test_getDB');

Route::any('/myclass', function (){
	return view('test_socket');
});


// Route::post('/testSendUser', 'UserController@sendReminderEmail');

// Route::get('/artisanCon', 'artisan_Controller');

