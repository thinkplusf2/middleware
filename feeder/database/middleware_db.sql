-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2018 at 11:58 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `middleware_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `gameCode` varchar(100) NOT NULL,
  `nameGame` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`gameCode`, `nameGame`) VALUES
('1g46b5zx7us6r', 'Monkey 3D Plus'),
('1q36p58phmt6y', 'Genie'),
('35wk8k7eigzk6', 'Fish Hunter'),
('7ufj5fcktqre1', 'Golden Shark'),
('8rqwot18etnuw', 'Thunder God'),
('ebudnqj68h6d4', 'Happy Party'),
('frgioi1zktkww', 'Monkey 3D'),
('fwria11mjbrwh', 'Three Kingdoms Quest'),
('i6mhf1gj358ha', 'Pirate King'),
('jtid4pjkfcwn1', 'Magician Journey'),
('kf41ymtxfos1r', 'Ocean Paradise'),
('mj6n6j3rrwpbs', 'Monkey ThunderBolt'),
('sgkhbusx3ughr', 'Lucky Roulette'),
('xico36z59dh9h', 'Laura Quest'),
('znxcuoqga9nck', 'Super Dragon');

-- --------------------------------------------------------

--
-- Table structure for table `jackpot`
--

CREATE TABLE `jackpot` (
  `Amount` float NOT NULL,
  `timeStampJackpot` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jackpot`
--

INSERT INTO `jackpot` (`Amount`, `timeStampJackpot`) VALUES
(505452, '2018-01-09 10:47:04');

-- --------------------------------------------------------

--
-- Table structure for table `jackpotgamesjoker`
--

CREATE TABLE `jackpotgamesjoker` (
  `GameCode` varchar(100) NOT NULL,
  `Amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jackpotgamesjoker`
--

INSERT INTO `jackpotgamesjoker` (`GameCode`, `Amount`) VALUES
('\'79mafnrjt48aa\'', 1655.17),
('\'k3anse3yrrunq\'', 1655.17),
('\'pirtanombyroh\'', 1655.17),
('\'ne4gq55cpitgg\'', 1655.17),
('\'4d5kdkpqi6sk4\'', 992.6),
('\'u6d7fsg355x7a\'', 992.6),
('\'5864tji8w113w\'', 992.6),
('\'naagsa5ycfugq\'', 992.6),
('\'kia1eetdryo1c\'', 992.6),
('\'u17q53q45xcp1\'', 992.6),
('\'1q36p58phmt6y\'', 992.6),
('\'9xpa7brfxj7zo\'', 992.6),
('\'byz81hmsq748k\'', 992.6),
('\'tqi9778i7mi6o\'', 992.6),
('\'fwria11mjbrwh\'', 754.34),
('\'8rqwot18etnuw\'', 754.34),
('\'dhdirsn3m3xia\'', 754.34),
('\'kf41ymtxfos1r\'', 754.34),
('\'ebudnqj68h6d4\'', 754.34),
('\'i4rc816e388c6\'', 1007.42),
('\'xtpy4bx49xhx1\'', 1007.42),
('\'nh9swadbc3use\'', 1007.42),
('\'ruufkzk1kpefn\'', 1007.42),
('\'awn5jciusna5c\'', 1007.42),
('\'ef1uyxt98o6ur\'', 1197.88),
('79mafnrjt48aa', 1019.29);

-- --------------------------------------------------------

--
-- Table structure for table `transactionjoker`
--

CREATE TABLE `transactionjoker` (
  `idTranGame` int(100) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `OCode` text NOT NULL,
  `GameCode` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `Type` text NOT NULL,
  `Amount` float NOT NULL,
  `Result` float NOT NULL,
  `Time` varchar(200) NOT NULL,
  `AppID` text NOT NULL,
  `CurrencyCode` text NOT NULL,
  `Details` text NOT NULL,
  `timeZone` text NOT NULL,
  `EndDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactionjoker`
--

INSERT INTO `transactionjoker` (`idTranGame`, `Username`, `OCode`, `GameCode`, `Description`, `Type`, `Amount`, `Result`, `Time`, `AppID`, `CurrencyCode`, `Details`, `timeZone`, `EndDate`) VALUES
(355, 'TOPTEN', 'qqo84f71qni56', 'mj6n6j3rrwpbs', 'Main Game', 'Main', 1.5, 0, '2017-12-27T00:20:09.179598', 'TF39', 'THB', '{\"Bets\":[1,0,0,0,0,1,0,10,0,1,0,0,1,0,1],\"GeneralBets\":[0,0,0],\"General\":2,\"Winner\":3,\"RunnerUp\":1,\"Rates\":[5,8,10,125,175,4,60,80,30,3,1000,500,200,20,100,2,9,2],\"GeneralRates\":[2,9,2]}', 'SA%20Western%20Standard%20Time', '2017-12-27 01:28:00'),
(356, 'TOPTEN', 'qqoexutn71i56', 'mj6n6j3rrwpbs', 'Main Game', 'Main', 2.6, 5, '2017-12-27T00:28:11.813246', 'TF39', 'THB', '{\"Bets\":[1,0,0,0,0,1,0,10,0,1,0,0,1,10,1],\"GeneralBets\":[0,1,0],\"General\":2,\"Winner\":4,\"RunnerUp\":2,\"Rates\":[175,30,1000,125,3,20,4,5,60,500,200,80,100,8,10,2,9,2],\"GeneralRates\":[2,9,2]}', 'SA%20Western%20Standard%20Time', '2017-12-27 01:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `transactionwallet`
--

CREATE TABLE `transactionwallet` (
  `idtranWall` int(100) NOT NULL,
  `timeAct` text NOT NULL,
  `action` text NOT NULL,
  `balance` float NOT NULL,
  `username` varchar(100) NOT NULL,
  `idWall` int(100) NOT NULL,
  `totalAmount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transactotaljoker`
--

CREATE TABLE `transactotaljoker` (
  `idTranTotalGame` int(100) NOT NULL,
  `Date` text NOT NULL,
  `PersonOCode` text NOT NULL,
  `Username` text NOT NULL,
  `CurrenyCode` text NOT NULL,
  `TotalAmount` float NOT NULL,
  `TotalResult` float NOT NULL,
  `TotalDeposit` float NOT NULL,
  `TotalWithdraw` float NOT NULL,
  `EndDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactotaljoker`
--

INSERT INTO `transactotaljoker` (`idTranTotalGame`, `Date`, `PersonOCode`, `Username`, `CurrenyCode`, `TotalAmount`, `TotalResult`, `TotalDeposit`, `TotalWithdraw`, `EndDate`) VALUES
(6, '2018-01-06T00:00:00', 'xj8citb8ucwre', 'TF39.SWIN0SE', 'THB', 1287, 697.8, 7100, 4100, '2018-01-08'),
(7, '2018-01-08T00:00:00', 'xj8cixxypqdre', 'TF39.SWIN010', 'THB', 1340.2, 222.9, 0, 0, '2018-01-09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(20) NOT NULL,
  `password` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`) VALUES
('SWIN010', NULL),
('TF39M', NULL),
('TOPTEN', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `idWallet` int(10) NOT NULL,
  `currenyCode` varchar(5) NOT NULL,
  `balance` float NOT NULL,
  `username` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`idWallet`, `currenyCode`, `balance`, `username`, `type`) VALUES
(1, 'THB', 10000, 'TOPTEN', 'joker'),
(2, 'CNY', 5000, 'SWIN010', 'cockfight'),
(5, 'THB', 3002, 'TOPTEN', 'cockfight'),
(6, 'THB', 2000, 'TF39M', 'cockfight'),
(7, 'THB', 2001.1, 'SWIN010', 'joker'),
(8, 'THB', 1000, 'TF39M', 'joker');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`gameCode`);

--
-- Indexes for table `transactionjoker`
--
ALTER TABLE `transactionjoker`
  ADD PRIMARY KEY (`idTranGame`),
  ADD KEY `transactionjoker_ibfk_1` (`Username`),
  ADD KEY `transactionjoker_ibfk_2` (`GameCode`);

--
-- Indexes for table `transactionwallet`
--
ALTER TABLE `transactionwallet`
  ADD PRIMARY KEY (`idtranWall`),
  ADD KEY `transactionwallet_ibfk_1` (`username`);

--
-- Indexes for table `transactotaljoker`
--
ALTER TABLE `transactotaljoker`
  ADD PRIMARY KEY (`idTranTotalGame`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`idWallet`),
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transactionjoker`
--
ALTER TABLE `transactionjoker`
  MODIFY `idTranGame` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=357;

--
-- AUTO_INCREMENT for table `transactionwallet`
--
ALTER TABLE `transactionwallet`
  MODIFY `idtranWall` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactotaljoker`
--
ALTER TABLE `transactotaljoker`
  MODIFY `idTranTotalGame` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `idWallet` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
