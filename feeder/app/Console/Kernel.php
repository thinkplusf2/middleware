<?php 

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // 'App\Console\Commands\emailUser' ,
        // 'App\Console\Commands\JokerTransactionApiWrapper',
        'App\Console\Commands\CockTransactionScheduler',
        'App\Console\Commands\JokerTransactionScheduler',
        'App\Console\Commands\WebSocketServer'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {   
        // echo "schedule  ";
        // $schedule->command('Scheduler:JokerTransaction')->everyMinute();
        // $schedule->command('Scheduler:CockTransaction')->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
