<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

// use App\Http\Middleware\Wrapper\JokerTransactionApiWrapper;

use DateTime;

class emailUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command will write file log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "RetrieveTransaction ";

        $start_date = "2016-12-01";
        $end_date   = "2016-12-01";
        $username = "Username01";
        $service = new JokerTransactionApiWrapper();
        // $data = $service->RetrieveTransaction($start_date, $end_date);
        $data = $service->TransactionTotal($username);
        // $data = $service->GetJackpot();

        Log::info('message', array('data' => $data));
    }

    // public function CallTransactionTotal ()
    // {
    //     echo "CallTransactionTotal ";

    //     $start_date = "2016-12-01";
    //     $end_date   = "2016-12-01";
    //     $username = "Username01";
    //     $service = new JokerTransactionApiWrapper();
    //     $data = $service->TransactionTotal($username);
    //     Log::info('message', array('data' => $data));
    // }

    // public function CallGetJackpot ()
    // {
    //     echo "CallGetJackpot ";

    //     $start_date = "2016-12-01";
    //     $end_date   = "2016-12-01";
    //     $username = "Username01";
    //     $service = new JokerTransactionApiWrapper();
    //     $data = $service->GetJackpot();
    //     Log::info('message', array('data' => $data));
    // }

    // public function CallGetJackpotGames ()
    // {
    //     echo "CallGetJackpotGames ";

    //     $start_date = "2016-12-01";
    //     $end_date   = "2016-12-01";
    //     $username = "Username01";
    //     $service = new JokerTransactionApiWrapper();
    //     $data = $service->GetJackpotGames();
    //     Log::info('message', array('data' => $data));
    // }
}
