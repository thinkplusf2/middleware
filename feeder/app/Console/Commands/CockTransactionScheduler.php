<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

// use App\Http\Middleware\Wrapper\JokerTransactionApiWrapper;

use DateTime;

class CockTransactionScheduler extends Command
{

    private $config = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Scheduler:CockTransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'No discription.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Fetching data from database.";

        $cert = "hi5BDE7RJgIzYzZ9";
        $user = "hthb0101";
        $lastupdatedate = "0";
        $startdate = "27/07/2016";
        $enddate = "27/07/2016"; 

        $this->config["tracsaction"]["cert"] = "hi5BDE7RJgIzYzZ9";
        $this->config["tracsaction"]["user"]   = "hthb0101";

        $this->config["tracsaction"]["lastupdatedate"] = "0";
        $this->config["tracsaction"]["nanos"] = "2";
        $this->config["tracsaction"]["extension1"] = "";
        $this->config["tracsaction"]["extension2"] = "";
        $this->config["tracsaction"]["extension3"] = "";

        $this->config["tracsaction"]["startdate"] = "27/07/2016";
        $this->config["tracsaction"]["enddate"] = "27/07/2016";

        $this->GetTransacLastUpdateDate();
        $this->GetTransacEventDate();
    }


    public function GetTransacLastUpdateDate() {
        $dataArray = array();

        if (!empty($this->config["tracsaction"]["cert"]))   $dataArray['cert'] = $this->config["tracsaction"]["cert"];

        if (!empty($this->config["tracsaction"]["user"]))   $dataArray['user'] = $this->config["tracsaction"]["user"];

        if (!is_null($this->config["tracsaction"]["lastupdatedate"])) $dataArray['lastupdatedate'] = $this->config["tracsaction"]["lastupdatedate"];

        // recieve optional variable
        if (!is_null($this->config["tracsaction"]["nanos"])) $dataArray['nanos'] = $this->config["tracsaction"]["nanos"];

        if (!is_null($this->config["tracsaction"]["extension1"])) $dataArray['extension1'] = $this->config["tracsaction"]["extension1"];

        if (!is_null($this->config["tracsaction"]["extension2"])) $dataArray['extension2'] = $this->config["tracsaction"]["extension2"];

        if (!is_null($this->config["tracsaction"]["extension3"])) $dataArray['extension3'] = $this->config["tracsaction"]["extension3"];
        
        // echo "Start retrieve transaction at date '$startdate' to '$enddate'";
        // var_dump($dataArray);

        $wrapper = new ServiceTransacCockWrapper();
        $wrapper->GetTransacLastUpdateDate($dataArray);

        // echo "Finish retrieve transaction at date '$startdate' to '$enddate'";
    }

     public function GetTransacEventDate() {
        $dataArray = array();
        $startdateEventDate = "";
        $username   = "";

        if (!empty($this->config["tracsaction"]["cert"]))   $dataArray['cert'] = $this->config["tracsaction"]["cert"];

        if (!empty($this->config["tracsaction"]["user"]))   $dataArray['user'] = $this->config["tracsaction"]["user"];

        if (!empty($this->config["tracsaction"]["startdate"]))  $dataArray['startdate'] = $this->config["tracsaction"]["startdate"];

        if (!empty($this->config["tracsaction"]["enddate"]))    $dataArray['enddate'] = $this->config["tracsaction"]["enddate"];

        if (!is_null($this->config["tracsaction"]["extension1"])) $dataArray['extension1'] = $this->config["tracsaction"]["extension1"];

        if (!is_null($this->config["tracsaction"]["extension2"])) $dataArray['extension2'] = $this->config["tracsaction"]["extension2"];

        if (!is_null($this->config["tracsaction"]["extension3"])) $dataArray['extension3'] = $this->config["tracsaction"]["extension3"];
        
        $wrapper = new ServiceTransacCockWrapper();
        $wrapper->GetTransacEventDate($dataArray);

    }

}
