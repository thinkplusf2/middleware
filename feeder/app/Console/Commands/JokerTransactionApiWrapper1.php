<?php 
namespace App\Console\Commands; 
use Illuminate\Console\Command;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

use DateTime;

class JokerTransactionApiWrapper1 
{
	var $url_joker = 'http://api.joker688.net:81/';
	var $secret = 'qpdcrnoddr7m6';

	public function RetrieveTransaction($StartDate, $EndDate){
		// Send to api server 
		$client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp= $date->getTimestamp();

        $dataArray  = array(
            'Method' => "TS",
            'StartDate' => $StartDate,
            'EndDate' => $EndDate,
            'Timestamp' => $timestamp
        );

        $post_data = array(
            'form_params' => array($dataArray)
            );        
        
        $signature = $this->signature_gen($this->secret,$dataArray);
        // var_dump($post_data);

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
		return $data;
	}

	public function TransactionTotal($username){
        $client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp= $date->getTimestamp();

        $dataArray  = array(
            'Method' => "TRX",
            'StartDate' => "2016-12-01",
            'Timestamp' => "$timestamp",
   	        'Username' => $username
        );

        $post_data = array(
            'form_params' => array($dataArray)
            );

        $signature = $this->signature_gen($this->secret,$dataArray);

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
		return $data;

    }

    public function GetJackpot(){
        $client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp= $date->getTimestamp();

        $dataArray  = array(
            'Method' => "JP",
            'Timestamp' => "$timestamp"
        );

        $post_data = array(
            'form_params' => array($dataArray)
            );

        $signature = $this->signature_gen($this->secret,$dataArray);

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
		return $data;
    }

    public function GetJackpotGames(){
        $client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp= $date->getTimestamp();

        $dataArray  = array(
            'Method' => "GJP",
            'Timestamp' => "$timestamp"
        );

        $post_data = array(
            'form_params' => array($dataArray)
            );

        $signature = $this->signature_gen($this->secret,$dataArray);

        $res=  $client->post(
            $this->url_joker."?AppId=TF39&Signature=".$signature, $post_data  
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
		return $data;
    }

	public function signature_gen($secretF,$key){
        $text = "";
        ksort($key);
        while ($key_name = current($key)) {           
                $text .= key($key).'='.$key_name.'&';            
            next($key);
        }

        $text = substr($text,0,-1); 
        var_dump($text);
        echo "<br>";
        var_dump($secretF);
        echo "<br>";
        $signature = base64_encode(hash_hmac("SHA1",$text, $secretF, true));
        echo "<br>";
        var_dump($signature);
        echo "<br>";
        //$signature = base64_encode(hash_hmac("SHA1",$key, $this->secret));
         //return substr( $signature, 0, -2);
        return  $signature;

    }
}
?>