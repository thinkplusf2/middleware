<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

use DateTime;

class ServiceTransacCockWrapper
{ 
    var $url_joker = 'http://203.69.34.177:7749/api/hiwinbet/';

    public function GetTransacLastUpdateDate (Request $request){
        $cert = $request->input('cert');
        $user = $request->input('user');
        $lastupdatedate = $request->input('lastupdatedate');

        $dataArray  = array(
            'cert' => $cert,
            'user' => $user,
            'lastupdatedate' => $lastupdatedate
        );

        // recieve optional variable
        $nanos = $request->input('nanos');
        if (!is_null($nanos)) $dataArray['nanos'] = $nanos;

        $extension1 = $request->input('extension1');
        if (!is_null($extension1)) $dataArray['extension1'] = $extension1;

        $extension2 = $request->input('extension2');
        if (!is_null($extension2)) $dataArray['extension2'] = $extension2;

        $extension3 = $request->input('extension3');
        if (!is_null($extension3)) $dataArray['extension3'] = $extension3;

        $url_GetTranUpdate = $this->url_joker."getTransactionByLastUpdateDate";
        $client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
        $post_data = array(
            'form_params' => $dataArray
            );        
        
        $res=  $client->post(
            $url_GetTranUpdate, $post_data              
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
        $result = array("data" =>$data["status"]);
        return $data;

    }

    public function GetTransacEventDate (Request $request){
        $cert = $request->input('cert');
        $user = $request->input('user');
        $startdate = $request->input('startdate');
        $enddate = $request->input('enddate');

        // recieve optional variable
        $extension1 = $request->input('extension1');
        if (!is_null($extension1)) $dataArray['extension1'] = $extension1;

        $extension2 = $request->input('extension2');
        if (!is_null($extension2)) $dataArray['extension2'] = $extension2;

        $extension3 = $request->input('extension3');
        if (!is_null($extension3)) $dataArray['extension3'] = $extension3;

        $url_GetTranUpdate = $this->url_joker."getTransactionByEventDate";
        $client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp= $date->getTimestamp();

        $dataArray  = array(
            'cert' => $cert,
            'user' => $user,
            'startdate' => $startdate,
            'enddate' => $enddate,
            'extension1' => $extension1,
            'extension2' => $extension2,
            'extension3' => $extension3
        );

        $post_data = array(
            'form_params' => $dataArray
            );        
        
        $res=  $client->post(
            $url_GetTranUpdate, $post_data              
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
        $result = array("data" =>$data["status"]);
        return $data;
    }

}
