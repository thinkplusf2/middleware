<?php  

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

// use App\Http\Middleware\Wrapper\JokerTransactionApiWrapper;

use DateTime;

class JokerTransactionScheduler extends Command
{

    private $config = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Scheduler:JokerTransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'No discription.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Fetching data from database.";
        $this->config["tracsaction"]["startdate"] = "2016-10-15 11:57";
        $this->config["tracsaction"]["enddate"]   = "2016-12-30 11:57";

        $this->config["tracsaction"]["startdateTotal"] = "2016-12-01";
        $this->config["tracsaction"]["enddateTotal"] = "2016-12-21";
        $this->config["tracsaction"]["username"] = " hthb0101";

        $this->RetreiveTransaction();
        $this->TransactionTotal();
        $this->GetJackpot();
        $this->GetJackpotGames();
    }


    public function RetreiveTransaction() {
        $startdate = "";
        $enddate   = "";

        if (!empty($this->config["tracsaction"]["startdate"]))
            $startdate = $this->config["tracsaction"]["startdate"];

        if (!empty($this->config["tracsaction"]["enddate"]))
            $enddate = $this->config["tracsaction"]["enddate"];
        
        // echo "Start retrieve transaction at date '$startdate' to '$enddate'";

        $wrapper = new JokerTransactionApiWrapper();
        $wrapper->RetrieveTransaction($startdate, $enddate);

        // echo "Finish retrieve transaction at date '$startdate' to '$enddate'";
    }

     public function TransactionTotal() {
        $startdateTotal = "";
        $username   = "";
        $enddateTotal = "";

        if (!empty($this->config["tracsaction"]["startdateTotal"]))
            $startdateTotal = $this->config["tracsaction"]["startdateTotal"];

        if (!empty($this->config["tracsaction"]["username"]))
            $username = $this->config["tracsaction"]["username"];

        if (!empty($this->config["tracsaction"]["enddateTotal"]))
            $enddateTotal = $this->config["tracsaction"]["enddateTotal"];
        
        $wrapper = new JokerTransactionApiWrapper();
        $wrapper->TransactionTotal($username, $startdateTotal, $enddateTotal);

    }

    public function GetJackpot() {        
        $wrapper = new JokerTransactionApiWrapper();
        $wrapper->GetJackpotGames($username, $StartDate);

    }
    public function GetJackpotGames()
    {        
        $wrapper = new JokerTransactionApiWrapper();
        $wrapper->GetJackpotGames($username, $StartDate);
    }


}
