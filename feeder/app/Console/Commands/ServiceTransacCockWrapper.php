<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

use DateTime;
class ServiceTransacCockWrapper extends Command
{

    protected $signature = 'JokerWrap:ServiceTransacCock';
    var $url_joker = 'http://203.69.34.177:7749/api/hiwinbet/';

    protected $description = 'Command description';
    

    public function GetTransacLastUpdateDate ($dataArray){
        $url_GetTranUpdate = $this->url_joker."getTransactionByLastUpdateDate";
        $client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp= $date->getTimestamp();
        $post_data = array(
            'form_params' => $dataArray
            );        
        
        $res=  $client->post(
            $url_GetTranUpdate, $post_data              
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
        $result = array("data" =>$data["status"]);
        // var_dump($data);
        return $data;

    }

    public function GetTransacEventDate ($dataArray){
        $url_GetTranUpdate = $this->url_joker."getTransactionByEventDate";
        $client = new \GuzzleHttp\Client();
        $date = new DateTime();
        $timestamp= $date->getTimestamp();

        $post_data = array(
            'form_params' => $dataArray
            );        
        
        $res=  $client->post(
            $url_GetTranUpdate, $post_data              
        );

        $status_code = $res->getStatusCode();
        $data = json_decode($res->getBody(),true);
        $result = array("data" =>$data["status"]);
        // var_dump($data);

        return $data;
    }

}
