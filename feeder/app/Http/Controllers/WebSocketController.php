<?php

namespace App\Http\Controllers;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Illuminate\Support\Facades\Log;

class WebSocketController extends Controller implements MessageComponentInterface {

    private $connections = [];
    var $url_joker = "http://54.179.168.96/middleware/app/joker/wallet/";
    var $url_joker2 = "ws://54.255.196.238:8080/LNXMVMainApp/fmxlnxhdpboardendpoint";
    var $url_CF = "";

    /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    function onOpen(ConnectionInterface $conn) {

        \Ratchet\Client\connect($this->url_joker)->then(function($conn) {

            $conn->send('{"sender":"jkrwatcher","action":"cnswatcher_alert_in"}'); // watcher 
            $conn->on('message', function($msg) use ($conn) {
                $strMsg = json_decode($msg, true);
                $resultCall = $this->callguzle_TT_func($conn, $msg);
            });
        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });


        // echo "string onOpen WebSocketController";
        // $this->connections[$conn->resourceId] = compact('conn') + ['user_id' => null];
        // $fromUserId = $this->connections[$conn->resourceId]['user_id'];
        //     $array = '{"sender":"jkrwatcher","action":"cnswatcher_alert_in"} ';
        //     $conn->send(json_encode($array));
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    function onClose(ConnectionInterface $conn) {
        $disconnectedId = $conn->resourceId;
        unset($this->connections[$disconnectedId]);
        foreach ($this->connections as &$connection)
            $connection['conn']->send(json_encode([
                'offline_user' => $disconnectedId,
                'from_user_id' => 'server control',
                'from_resource_id' => null
            ]));
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    function onError(ConnectionInterface $conn, \Exception $e) {
        $userId = $this->connections[$conn->resourceId]['user_id'];
        echo "An error has occurred with user $userId: {$e->getMessage()}\n";
        unset($this->connections[$conn->resourceId]);
        $conn->close();
    }

    /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $conn The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    function onMessage(ConnectionInterface $conn, $msg) {

        if (is_null($this->connections[$conn->resourceId]['user_id'])) {
            $this->connections[$conn->resourceId]['user_id'] = $msg;
            $onlineUsers = [];
            foreach ($this->connections as $resourceId => &$connection) {
                $resultCall = $this->callguzle_TT_func($conn, $msg);
                /* $connection['conn']->send(json_encode(["result" => true])); */
                if ($resultCall)
                    $connection['conn']->send(json_encode(["success" => true]));
                else
                    $connection['conn']->send(json_encode(["success" => false]));

                if ($conn->resourceId != $resourceId)
                    $onlineUsers[$resourceId] = $connection['user_id'];
            }

            $conn->send(json_encode(['online_users' => $onlineUsers]));
        } else {
            $resultCall = $this->callguzle_TT_func($conn, $msg);

            $fromUserId = $this->connections[$conn->resourceId]['user_id'];
            $msg = json_decode($msg, true);

            $this->connections[$msg['to']]['conn']->send(json_encode([
                'msg' => ["success" => $resultCall],
                'from_user_id' => $fromUserId,
                'from_resource_id' => $conn->resourceId
            ]));
            Log::info(array('msg' => ["success" => $resultCall],
                'from_user_id' => $fromUserId,
                'from_resource_id' => $conn->resourceId));
        }
    }

    function callguzle_TT_func($conn, $msg) {
        $strMsg = json_decode($msg, true);
        // var_dump($strMsg);
        $strFunc = "";

        $client = new \GuzzleHttp\Client(['http_errors' => false]);

//        if ( $strMsg['sender']== "cockfight") { // call cockfight function 
//            $strFunc = "cockfight";
//            if ($strMsg['action'] == "transferin") { // call transferin function 
//                $strFunc .= "_transferin";
//
//
//            } else { // call transferout function 
//                $strFunc .= "_transferout";
//
//            }
//        } else if ($strMsg['sender'] == "joker") { // call joker function 
//            $strFunc = "joker";
//            if ($strMsg['action'] == "transferin") { // call transferin function 
//                $strFunc .= "_transferin";
//                $dataArray  = array(
//                    'username' => $strMsg['username'],
//                    'amount' => $strMsg['credit'],
//                    'type' => $strMsg['sender']
//                );
//            
//                $post_data = array(
//                    'form_params' => $dataArray 
//                );
//
//                $res=  $client->post(
//                    $this->url_joker."deposit", $post_data              
//                );
//                $status_code = $res->getStatusCode();
//                $data = json_decode($res->getBody(),true);
//                // var_dump(array($res));
//            } else { // call transferout function 
//                $strFunc .= "_transferout";
//                $dataArray  = array(
//                    'username' => $strMsg['username'],
//                    'amount' => $strMsg['credit'],
//                    'type' => $strMsg['sender']
//                );
//            
//                $post_data = array(
//                    'form_params' => $dataArray 
//                );
//                
//                $res=  $client->post(
//                    $this->url_joker."withDraw", $post_data              
//                );
//                $status_code = $res->getStatusCode();
//                $data = json_decode($res->getBody(),true);
//                // var_dump(array($res));
//
//            }
//
//        }
//
//        Log::info(array('func' => $strFunc, 'msg' => $msg)); 
//
//        if (isset($data["error"]) ) {
//            return (false);
//        } else return (true);


        if ($strMsg['action'] == "jkrtransfer") { // call joker function
            if ($strMsg['type'] == "IN") { // call transferin function 
                $strFunc .= "_transferIN";
                $dataArray = array(
                    'username' => $strMsg['username'],
                    'amount' => $strMsg['credit'],
                    'type' => $strMsg['sender']
                );

                $post_data = array(
                    'form_params' => $dataArray
                );

                $res = $client->post(
                        $this->url_joker . "deposit", $post_data
                );
                $status_code = $res->getStatusCode();
                $data = json_decode($res->getBody(), true);

                Log::info(array('func' => $strFunc, 'msg' => $msg));

                if (isset($data["error"])) {
                    $res_mes = '{"sender":"jkrwatcher","action":"jkrtransfer","type":"IN","result":"false","username":"' . $data['Username'] . '","credit":"' . $strMsg['credit'] . '"}';
                } else {
                    $res_mes = '{"sender":"jkrwatcher","action":"jkrtransfer","type":"IN","result":"true","username":"' . $data['Username'] . '","credit":"' . $strMsg['credit'] . '"}';
                }


                return $res_mes;
            } else if ($strMsg['type'] == "OUT") {  // call transferout function 
                $strFunc .= "_transferOUT";
                $dataArray = array(
                    'username' => $strMsg['username'],
                    'amount' => $strMsg['credit'],
                    'type' => $strMsg['sender']
                );

                $post_data = array(
                    'form_params' => $dataArray
                );

                $res = $client->post(
                        $this->url_joker . "withDraw", $post_data
                );
                $status_code = $res->getStatusCode();
                $data = json_decode($res->getBody(), true);
                
                Log::info(array('func' => $strFunc, 'msg' => $msg));
                if (isset($data["error"])) {
                    $res_mes = '{"sender":"jkrwatcher","action":"jkrtransfer","type":"OUT","result":"false","username":"' . $data['Username'] . '","credit":"' . $strMsg['credit'] . '"}';
                } else {
                    $res_mes = '{"sender":"jkrwatcher","action":"jkrtransfer","type":"OUT","result":"true","username":"' . $data['Username'] . '","credit":"' . $strMsg['credit'] . '"}';
                }
                return $res_mes;
            }else if($strMsg['action']=="joker"){
                
                if($strMsg['subact']=="transferout"){
                    if($strMsg['result']=="success"){
                        
                    }
                }else if($strMsg['subact']=="transferout"){
                    if($strMsg['result']=="i_am_ok"){
                        
                    }
                }
            }
        } else { // call cf function
            
        }
    }

}
