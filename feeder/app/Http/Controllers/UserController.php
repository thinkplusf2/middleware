<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Jobs\SendReminderEmail;
use App\Http\Controllers\Controller;
use Middleware\Wrapper\JokerTransactionApiWrapper;

class UserController extends Job implements ShouldQueue
{
    /**
     * Send a reminder e-mail to a given user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function sendReminderEmail(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $job = (new SendReminderEmail($user))->delay(5);

        $this->dispatch($job);


        // Call RetrieveTrasaction 
        $start_date = "2016-12-01";
        $end_date   = "2016-12-01";

        $service = new JokerTransactionApiWrapper();
        $data = $service->RetrieveTransaction($start_date, $end_date);
        $log->write($data);

    }
}