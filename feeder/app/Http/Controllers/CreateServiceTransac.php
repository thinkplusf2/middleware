<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DateTime;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use \Monolog\Formatter\LineFormatter;

class CreateServiceTransac extends Controller {

    var $secret = 'qpdcrnoddr7m6';
    var $url_joker = 'http://api.joker688.net:81';

    public function test_null(Request $request) {
        date_default_timezone_set("Asia/Bangkok");
        $StartDate = $request->input('StartDate');
        $EndDate = $request->input('EndDate');
        if ($StartDate == null || $EndDate == null) {
            $lastRecod = DB::table('transactionjoker')->select('*')->orderBy('idTranGame', 'DESC')->first();

            // $temp_time = strtotime(str_replace('T', ' ', $lastRecod->Time)); 
            $temp_time = date("Y-m-d H:i");
            //$StartDate = date('Y-m-d H:i', $temp_time);
            $StartDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60 + 60 * 60);
            //echo $StartDate . " " . $EndDate;
            $timeZone = "SA%20Western%20Standard%20Time";
        } else {
            $StartDate = date('Y-m-d H:i', strtotime($StartDate) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($EndDate) + 60 * 60);
        }
        echo $StartDate . " :: " . $EndDate;
    }

    public function testdb_null() {
        $lastRecod = DB::table('users')->select('*')->where('username', '=', 'toptenma')->first();

        if (!is_null($lastRecod)) {
            echo "topten0";
            var_dump($lastRecod);
        } else {
            echo "NO";
        }
    }

    public function RetrieveTransaction(Request $request) {
        date_default_timezone_set("Asia/Bangkok");
        header('Content-Type: application/json');
        //date_default_timezone_set("Asia/Bangkok");
        //$username = $request->input("username");
        //$timeZone = $request->input('timeZone');
        $timeZone = "SA%20Western%20Standard%20Time";
        $StartDate = $request->input('StartDate');
        $EndDate = $request->input('EndDate');

        if ($StartDate == null || $EndDate == null) {
            //$lastRecod = DB::table('transactionjoker')->select('*')->orderBy('idTranGame', 'DESC')->first();
            // $temp_time = strtotime(str_replace('T', ' ', $lastRecod->Time)); 
            $temp_time = date("Y-m-d H:i");
            //$StartDate = date('Y-m-d H:i', $temp_time);
            $StartDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60 + 60 * 60);
            //echo $StartDate . " " . $EndDate;
        } else {
            $StartDate = date('Y-m-d H:i', strtotime($StartDate) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($EndDate) + 60 * 60);
        }

        do {
            // request to api server 
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            $dataArray = array(
                'EndDate' => $EndDate,
                'Method' => "TS",
                'StartDate' => $StartDate,
                'Timestamp' => $timestamp
            );

            $post_data = array(
                'form_params' => $dataArray
            );

            $signature = $this->signature_gen($this->secret, $dataArray);


//            $res = $client->post(
//                    $this->url_joker . "?AppId=TF39&Signature=" . $signature . "&timeZone=" . $timeZone, $post_data
//            );

            $res = $client->post(
                    $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data
            );

            $status_code = $res->getStatusCode();
        } while ($status_code != 200);

        // send data to DB 
        $data = json_decode($res->getBody(), true);
        $this->joker_log("RetrieveTransaction", "info", json_encode($dataArray), "Joker");
        $this->joker_log("RetrieveTransaction", "info", json_encode($data), "Joker");
        // Log::info('data', array('data' => $data));
        if (empty($data["data"])) {
            // Log::info('transactJokerMessage is null data', array('message' => $data));
        } else {

            $result = $data["data"]["Game"];
            //echo count($result)."<br>";
            //var_dump($result);
            for ($i = 0; $i < count($result); $i++) {
                // check record in DB
                $db_c_time = strtotime(str_replace('T', ' ', $result[$i]["Time"]));

                // $temp_time = strtotime(str_replace('T', ' ', $result[$i]["Time"])); 
                $db_r_time = date("Y-m-d H:i", $db_c_time);
                $checkRecordTransac = DB::table('transactionjoker')
                        ->select('idTranGame')
                        ->where('OCode', '=', $result[$i]["OCode"])
                        ->first();

                //var_dump($checkRecordTransac);
                //var_dump($result[$i]["Username"]);
                if (!is_null($checkRecordTransac)) { // update transaction JokerGame DB        !is_null($checkRecordTransac)    
//                        echo $result[$i]["Username"];
//                        echo ">>>> 11111111<br>";
                    $r_time = strtotime(str_replace('T', ' ', $result[$i]["Time"]));

                    // $temp_time = strtotime(str_replace('T', ' ', $result[$i]["Time"])); 
                    $c_time = date("Y-m-d H:i", $r_time);
                    $updateDB = DB::table('transactionjoker')
                            ->where('idTranGame', '=', $checkRecordTransac->idTranGame)
                            ->update(array(
                        'OCode' => $result[$i]["OCode"],
                        'Username' => $result[$i]["Username"],
                        'GameCode' => $result[$i]["GameCode"],
                        'Description' => $result[$i]["Description"],
                        'Type' => $result[$i]["Type"],
                        'Amount' => $result[$i]["Amount"],
                        'Result' => $result[$i]["Result"],
                        'Time' => $c_time,
                        'AppID' => $result[$i]["AppID"],
                        'CurrencyCode' => $result[$i]["CurrencyCode"],
                        'timeZone' => $timeZone,
                        //'EndDate' => $EndDate,
                        'Details' => $result[$i]["Details"]
                    ));
                    //Log::info('update transaction JokerGame ', array('update' => $result));
                } else {
// insert transaction JokerGame DB            

                    $r_time = strtotime(str_replace('T', ' ', $result[$i]["Time"]));

                    // $temp_time = strtotime(str_replace('T', ' ', $result[$i]["Time"])); 
                    $c_time = date("Y-m-d H:i", $r_time);
                    //$StartDate = date('Y-m-d H:i', $temp_time);

                    $insertDB = DB::table('transactionjoker')->insertGetId(array(
                        'OCode' => $result[$i]["OCode"],
                        'Username' => $result[$i]["Username"],
                        'GameCode' => $result[$i]["GameCode"],
                        'Description' => $result[$i]["Description"],
                        'Type' => $result[$i]["Type"],
                        'Amount' => $result[$i]["Amount"],
                        'Result' => $result[$i]["Result"],
                        'Time' => $c_time,
                        'AppID' => $result[$i]["AppID"],
                        'CurrencyCode' => $result[$i]["CurrencyCode"],
                        'timeZone' => $timeZone,
                        // 'EndDate' => $EndDate,
                        'Details' => $result[$i]["Details"]
                    ));
                    // Log::info('insert transaction JokerGame', array('insert' => $result));
                }

                // var_dump($checkRecordTransac); 
            }
        }
        $return = $data;
//        if ($username != null) {
//
//            $return = $this->transactionSummary($username, $StartDate, $EndDate);
//        } else {
//            $return = $data;
//        }
        //var_dump($data);
        echo json_encode($return);
    }

    public function TransactionTotal(Request $request) {
        header('Content-Type: application/json');

        ini_set('max_execution_time', 300);
        date_default_timezone_set("Asia/Bangkok");
        $EndDate = $request->input('EndDate');
        $StartDate = $request->input('StartDate');
        $userss = $request->input('user');
// Log::info( array('EndDate' => $EndDate,'userss' => $userss,'StartDate' => $StartDate) ); 
        if ($StartDate == null || $EndDate == null) {

            // $temp_time = strtotime(str_replace('T', ' ', $lastRecod->Time)); 
            $temp_time = date("Y-m-d H:i");
            //$StartDate = date('Y-m-d H:i', $temp_time);
            $StartDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($temp_time) + 60 * 60 + 60 * 60);
            //echo $StartDate . " " . $EndDate;
        } else {
            $StartDate = date('Y-m-d H:i', strtotime($StartDate) + 60 * 60);
            $EndDate = date('Y-m-d H:i', strtotime($EndDate) + 60 * 60);
        }

        $varlue = $this->sendRequestAndStoreDB($StartDate, $EndDate, $userss);

        echo json_encode($varlue);
    }

    public function sendRequestAndStoreDB($StartDate, $EndDate, $user) {


        date_default_timezone_set("Asia/Bangkok");

        do {
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            $dataArray = array(
                'EndDate' => $EndDate,
                'Method' => "TRX",
                'StartDate' => $StartDate,
                'Timestamp' => $timestamp,
                'Username' => $user
            );

            $post_data = array(/* $dataArray */
                'form_params' => $dataArray
            );

            $signature = $this->signature_gen($this->secret, $dataArray);

            $res = $client->post(
                    $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data
            );

            $status_code = $res->getStatusCode();
        } while ($status_code != 200);

        $data = json_decode($res->getBody(), true);

        $this->joker_log("RetrieveTransaction", "info", json_encode($dataArray), "Joker");
        $this->joker_log("RetrieveTransaction", "info", json_encode($data), "Joker");
        // var_dump($data);
        $result = $data["data"];
//        if (empty($result)) {
//            Log::info('transactTotalJokerMessage is null data', array('message' => $data));
//        } else {
//            for ($i = 0; $i < count($data); $i++) {
//                // check record in DB
//                $checkRecordTransac = DB::table('transacTotalJoker')
//                        ->select('idTranTotalGame')
//                        ->where([['Date', '=', $result[$i]["Date"]], ['PersonOCode', '=', $result[$i]["PersonOCode"]], ['Username', '=', $result[$i]["Username"]]])
//                        ->first();
//
//                /*  $query->where([
//                  ['column_1', '=', 'value_1'],
//                  ['column_2', '<>', 'value_2'],
//                  [COLUMN, OPERATOR, VALUE],
//                  ...
//                  ])
//                 */
//                if ($checkRecordTransac != null) { // update transaction JokerGame DB  
//                    $updateDB = DB::table('transacTotalJoker')
//                            ->where('idTranTotalGame', '=', $checkRecordTransac->idTranTotalGame)
//                            ->update(array(
//                        'Date' => $result[$i]["Date"],
//                        'PersonOCode' => $result[$i]["PersonOCode"],
//                        'Username' => $result[$i]["Username"],
//                        'CurrenyCode' => $result[$i]["CurrenyCode"],
//                        'TotalAmount' => $result[$i]["TotalAmount"],
//                        'TotalResult' => $result[$i]["TotalResult"],
//                        'TotalDeposit' => $result[$i]["TotalDeposit"],
//                        'TotalWithdraw' => $result[$i]["TotalWithdraw"],
//                        'EndDate' => $EndDate
//                    ));
//                    Log::info('update TransactionTotal JokerGame ', array('update' => $result));
//                } else { // insert transaction JokerGame DB     
//                    $insertDB = DB::table('transacTotalJoker')->insertGetId(array(
//                        'Date' => $result[$i]["Date"],
//                        'PersonOCode' => $result[$i]["PersonOCode"],
//                        'Username' => $result[$i]["Username"],
//                        'CurrenyCode' => $result[$i]["CurrenyCode"],
//                        'TotalAmount' => $result[$i]["TotalAmount"],
//                        'TotalResult' => $result[$i]["TotalResult"],
//                        'TotalDeposit' => $result[$i]["TotalDeposit"],
//                        'TotalWithdraw' => $result[$i]["TotalWithdraw"],
//                        'EndDate' => $EndDate
//                    ));
//                    Log::info('insert TransactionTotal JokerGame', array('insert' => $result));
//                }
//            }
//        }

        return ($data);
    }

    public function GetJackpot() {
        header('Content-Type: application/json');

        date_default_timezone_set("Asia/Bangkok");
        do {
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            $dataArray = array(
                'Method' => "JP",
                'Timestamp' => $timestamp
            );

            $post_data = array(
                'form_params' => $dataArray
            );

            $signature = $this->signature_gen($this->secret, $dataArray);

            $res = $client->post(
                    $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data
            );

            $status_code = $res->getStatusCode();
        } while ($status_code != 200);

        $data = json_decode($res->getBody(), true);
        $this->joker_log("GetJackpot", "info", json_encode($dataArray), "Joker");
        $this->joker_log("GetJackpot", "info", json_encode($data), "Joker");
        $result = $data;
        if (empty($result)) {
            // Log::info('GetJackpotJokerMessage is null data', array('message' => $data));
        } else { /* update transaction JokerGame DB */
            $insertDB = DB::table('jackpot')
                    ->insertGetId(array(
                'Amount' => $data["Amount"]
            ));
            //Log::info('update jackpot ', array('update' => $data));
        }
        echo json_encode($data);
    }

    public function GetJackpotGames() {
        header('Content-Type: application/json');

        date_default_timezone_set("Asia/Bangkok");
        do {
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            $dataArray = array(
                'Method' => "GJP",
                'Timestamp' => $timestamp
            );

            $post_data = array(
                'form_params' => $dataArray
            );

            $signature = $this->signature_gen($this->secret, $dataArray);

            $res = $client->post(
                    $this->url_joker . "?AppId=TF39&Signature=" . $signature, $post_data
            );

            $status_code = $res->getStatusCode();
        } while ($status_code != 200);

        $data = json_decode($res->getBody(), true);

        if (!empty($data["Message"])) {
            //Log::info('transactJokerMessage is null data', array('message' => $data));
        } else {
            $result = $data["Jackpots"];
            //var_dump(count($result));
            for ($i = 0; $i < count($result); $i++) {
                // check record in DB
                $checkRecordTransac = DB::table('jackpotgamesjoker')
                        ->select('GameCode')
                        ->where('GameCode', '=', $result[$i]["GameCode"])
                        ->first();

                if ($checkRecordTransac != null) { // update Jackpot JokerGame DB            
                    $updateDB = DB::table('jackpotgamesjoker')
                            ->where('GameCode', '=', $checkRecordTransac->GameCode)
                            ->update(array(
                        'GameCode' => $result[$i]["GameCode"],
                        'Amount' => $result[$i]["Amount"]
                    ));
                    //Log::info('update Jackpot JokerGame ', array('update' => $result));
                } else { // insert Jackpot JokerGame DB            
                    $insertDB = DB::table('jackpotgamesjoker')->insertGetId(array(
                        'GameCode' => $result[$i]["GameCode"],
                        'Amount' => $result[$i]["Amount"]
                    ));
                    //Log::info('insert Jackpot JokerGame', array('insert' => $result));
                }
                // var_dump($result[$i]);
            }
        }
        $array_result = array(
            "result" => "successed"
        );

        $this->joker_log("GetJackpotGames", "info", json_encode($dataArray), "Joker");
        $this->joker_log("GetJackpotGames", "info", json_encode($data), "Joker");
        echo json_encode($array_result);
    }

    public function transactionSummary(Request $request) {
        header('Content-Type: application/json');
        date_default_timezone_set("Asia/Bangkok");

        $endDate = $request->input('endDate');
        $date = $request->input('date');
        $userss = $request->input('username');

//        $startDate = date("Y-m-d H:i", strtotime($startDate));
//        $endDate = date("Y-m-d H:i", strtotime($endDate));
        if ($userss != null) {
            $transacSum = DB::table('transactionjoker')
                            ->select('*')
                            ->where('Username', '=', $userss)
                            ->where('Time', 'like', $date . "%")->get();

//            $transacSum = DB::select('select * from transactionjoker where Username = :Username and ( Time >= :start and Time <= :end )', ['Username' => $userss,'start' => $startDate,'end' => $endDate ]);
        } else {
            $transacSum = DB::table('transactionjoker')
                            ->select('*')
                            ->where('Time', 'like', $date . "%")->get();
        }

        if ($transacSum == null) {
            $res = array(
                "message" => "no data"
            );
        } else {
            $res = $transacSum;
        }

        echo json_encode($res);
    }

    public function signature_gen($secretF, $key) {
        //$this->secret = "qpdcrnoddr7m6"
        // $key must be array ;
        $text = "";
        ksort($key);
        while ($key_name = current($key)) {
            $text .= key($key) . '=' . $key_name . '&';
            next($key);
        }
        // again 

        $text = substr($text, 0, -1); // อั�?�?ี�?�?ือะ�?ร ตัดตัว & สุดท�?ายออ�?�?รั�? ok
//        var_dump($text);
//        echo "<br>";
//        var_dump($secretF);
//        echo "<br>";
        $signature = base64_encode(hash_hmac("SHA1", $text, $secretF, true));
//        echo "<br>";
//        var_dump($signature);
//        echo "<br>";
        //$signature = base64_encode(hash_hmac("SHA1",$key, $this->secret));
        //return substr( $signature, 0, -2);
        return $signature;
    }

    public function joker_log($funcName, $state, $details, $game) {
        $date = date('d-m-Y');
        $logfile = 'D:/log/joker/Log_Joker_' . $date . '.log';
        $logger = new Logger('Joker_log');
// Now add some handlers
        $logger->pushHandler(new StreamHandler($logfile, Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());
//        $msg = "[REFUND] [JOKER] Details : {TransactionID: tranID , PlayerID: player, Amount: amount, GameID: gameID, gtype: gtp}";
//        $msg = "[WIN] [JOKER] Details : { error_type : 401 ACCESS_DENIED, playerID : playerID }";
//        $msg = "[BALANCE] [JOKER] Details : { error_type : 402 INSUFFICIENT_FUNDS, playerID : playerID }";
//
//        $msg = "[BALANCE] [PORNHUB] Details : { error_type : 403 TOKEN_EXPIRED, playerID : playerID }";
//
//        $msg = "[BALANCE] [PORNHUB] Details : { error_type : 404 TOKEN_NOT_FOUND, playerID : playerID }";
//        $msg = "[TIP] [ERROR] [PORNHUB] Details : { error_type : 500 INTERNAL_SERVER_ERROR, playerID : playerID }";

        $msg = "[$funcName] [$game] Details : { $details }";
// You can now use your logger

        switch ($state) {
            case "info":
                $logger->info($msg . "\n");
                break;
            case "debug":
                $logger->debug($msg . "\n");
                break;
            case "alert":
                $logger->alert($msg . "\n");
                break;
            case "critical":
                $logger->critical($msg . "\n");
                break;
            case "emergency":
                $logger->EMERGENCY($msg . "\n");
                break;
            case "error":
                $logger->ERROR($msg . "\n");
                break;
            default:
        }
    }

}
