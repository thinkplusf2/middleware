<?php

namespace App\Jobs;

use App\User;
use App\Mail\SendEmailable;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Bus\Queueable;

use App\Http\Controllers\artisan_Controller;

class logfile_delay implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels,Queueable;

    public function __construct()
    {
       
    }

    public function handle(Mailer $mailer)
    {
       Mail::to('lapngponjira@gmail.com')->send(new SendEmailable()); 
    }
}